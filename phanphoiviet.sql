-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 27, 2017 at 05:39 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phanphoiviet`
--

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `ID` int(11) NOT NULL,
  `Name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `Origin` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ImageID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`ID`, `Name`, `Origin`, `ImageID`) VALUES
(1, 'Royal Dansk', 'Đan Mạch', 1),
(2, 'asdasdasd', 'dasdasda', 13);

-- --------------------------------------------------------

--
-- Stand-in structure for view `brand_view`
--
CREATE TABLE `brand_view` (
`ID` int(11)
,`Name` varchar(45)
,`Origin` varchar(45)
,`Path` varchar(255)
);

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `ID` int(11) NOT NULL,
  `Name` text COLLATE utf8_unicode_ci NOT NULL,
  `Email` text COLLATE utf8_unicode_ci,
  `Phone` text COLLATE utf8_unicode_ci,
  `Content` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`ID`, `Name`, `Email`, `Phone`, `Content`) VALUES
(1, 'Nguyễn Đức Vũ Anh', 'vuanhnguyenduc@gmail.com', '01693364060', 'Nhận thiết kế website, ứng dụng, chương trình quản lý....');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `ID` int(11) NOT NULL,
  `Path` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`ID`, `Path`) VALUES
(1, '/images/brand_1.png'),
(2, '/images/product_1.jpg'),
(3, '/images/product_2.jpg'),
(4, '/images/product_3.jpg'),
(5, '/images/product_4.jpg'),
(6, '/images/product_5.jpg'),
(7, '/images/product_6.jpg'),
(8, '/images/news_1.png'),
(9, '/images/news_2.png'),
(10, '/images/news_3.png'),
(11, '/images/partner_1.png'),
(12, '/images/slideImage_1.png'),
(13, '/images/brand_2.png'),
(14, '/images/product_7.png'),
(15, '/images/introduction_1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `introduction`
--

CREATE TABLE `introduction` (
  `ID` int(11) NOT NULL,
  `Title` text NOT NULL,
  `Content` text NOT NULL,
  `ImageID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `introduction`
--

INSERT INTO `introduction` (`ID`, `Title`, `Content`, `ImageID`) VALUES
(1, 'Introduction Number 1', '<p>sajdnsandkasndkjasndjsandsandnasjdnasndasjkn</p>\r\n<p>asdmkasmdlsamdklsamdkasmdlkasmkdlmasd</p>\r\n<p>askdaskdmkasmdkasmdklsmakdmaskldmkasmdlasmdklamsl</p>\r\n<p>&nbsp;</p>\r\n<p><img class="img-responsive" src="../../images/introduction_1.jpg" width="711" height="400" /></p>', 15);

-- --------------------------------------------------------

--
-- Stand-in structure for view `introduction_view`
--
CREATE TABLE `introduction_view` (
`ID` int(11)
,`Title` text
,`Content` text
,`Path` varchar(255)
);

-- --------------------------------------------------------

--
-- Table structure for table `new`
--

CREATE TABLE `new` (
  `ID` int(11) NOT NULL,
  `Title` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `AddedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Content` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `ImageID` int(11) NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `new`
--

INSERT INTO `new` (`ID`, `Title`, `AddedDate`, `Content`, `ImageID`, `Status`) VALUES
(1, 'Tin tức số 1', '2017-06-22 09:22:21', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc lobortis dapibus nulla, eget elementum erat vehicula a. Nullam maximus quam ut ex pharetra volutpat vitae auctor odio. Donec quis augue commodo, condimentum massa et, molestie odio. Maecenas erat est, sollicitudin rutrum lacus a, tincidunt vehicula velit. Suspendisse est metus, euismod quis varius vel, molestie at elit. Maecenas eros ligula, ullamcorper non est a, maximus posuere arcu. Etiam fringilla dapibus neque.<br /><br />Mauris felis nulla, vestibulum commodo lectus a, consectetur ultricies nulla. Morbi iaculis sagittis semper. Proin eget fringilla augue, eu lobortis felis. Etiam ultrices augue a metus aliquet tristique. Curabitur semper cursus scelerisque. Duis lacinia ex sapien, vitae maximus lacus volutpat ac. Vestibulum eget pretium urna. Sed sem enim, commodo et est vitae, elementum tempor nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam tristique sem vel tristique sagittis. Etiam porttitor interdum nisl, sit amet aliquam orci commodo vitae. Quisque egestas, diam eget ornare laoreet, quam nunc vestibulum erat, quis viverra urna ligula tempor diam. Donec tincidunt velit et est tincidunt scelerisque.<br /><br />Sed nec quam ut odio porttitor imperdiet. Aliquam erat volutpat. Fusce a lectus quis lorem maximus tempor vitae id elit. Vestibulum quis justo nulla. Etiam at tempor velit, tincidunt consectetur nunc. Donec erat risus, hendrerit non orci a, molestie faucibus purus. Morbi condimentum lorem mi, vitae varius enim eleifend sit amet. Sed gravida suscipit magna, vitae tristique urna bibendum at.<br /><br />Cras eu volutpat diam, et fringilla urna. Duis pellentesque volutpat quam, sed ultrices arcu commodo at. Nullam accumsan vehicula sem et cursus. Vivamus dui dui, mollis sed dictum non, condimentum vel lorem. Maecenas non augue vitae odio tempus placerat nec vitae lacus. Nam tortor diam, eleifend a fermentum ac, sagittis non erat. Etiam vitae egestas tortor, finibus rutrum lectus. Ut eget est nulla. Aliquam malesuada magna ut vulputate cursus. Aenean pretium volutpat massa, ac condimentum ipsum blandit ac.<br /><br />Mauris nec nulla et nisl posuere fermentum nec id tortor. Nam ac sagittis ipsum. Cras metus orci, posuere a efficitur quis, vehicula gravida turpis. Donec consequat in odio eget ullamcorper. Curabitur vestibulum, diam sed fermentum interdum, urna elit rutrum felis, a hendrerit lacus arcu a ipsum. Fusce vel fringilla erat. Proin est augue, vehicula cursus quam eu, ultrices vehicula lectus. Nunc condimentum orci felis, ac convallis odio ultrices id. Pellentesque mollis mauris sit amet nunc ullamcorper facilisis. Duis velit nibh, lacinia quis ex ac, rhoncus pellentesque quam. Cras commodo ac arcu in imperdiet. Pellentesque viverra luctus tellus nec tincidunt. Aliquam fringilla finibus justo, id congue nisl fringilla ac. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla rutrum viverra est.</p>', 8, 2),
(2, 'Tin tức số 2', '2017-06-22 09:22:50', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc lobortis dapibus nulla, eget elementum erat vehicula a. Nullam maximus quam ut ex pharetra volutpat vitae auctor odio. Donec quis augue commodo, condimentum massa et, molestie odio. Maecenas erat est, sollicitudin rutrum lacus a, tincidunt vehicula velit. Suspendisse est metus, euismod quis varius vel, molestie at elit. Maecenas eros ligula, ullamcorper non est a, maximus posuere arcu. Etiam fringilla dapibus neque.<br /><br />Mauris felis nulla, vestibulum commodo lectus a, consectetur ultricies nulla. Morbi iaculis sagittis semper. Proin eget fringilla augue, eu lobortis felis. Etiam ultrices augue a metus aliquet tristique. Curabitur semper cursus scelerisque. Duis lacinia ex sapien, vitae maximus lacus volutpat ac. Vestibulum eget pretium urna. Sed sem enim, commodo et est vitae, elementum tempor nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam tristique sem vel tristique sagittis. Etiam porttitor interdum nisl, sit amet aliquam orci commodo vitae. Quisque egestas, diam eget ornare laoreet, quam nunc vestibulum erat, quis viverra urna ligula tempor diam. Donec tincidunt velit et est tincidunt scelerisque.<br /><br />Sed nec quam ut odio porttitor imperdiet. Aliquam erat volutpat. Fusce a lectus quis lorem maximus tempor vitae id elit. Vestibulum quis justo nulla. Etiam at tempor velit, tincidunt consectetur nunc. Donec erat risus, hendrerit non orci a, molestie faucibus purus. Morbi condimentum lorem mi, vitae varius enim eleifend sit amet. Sed gravida suscipit magna, vitae tristique urna bibendum at.<br /><br />Cras eu volutpat diam, et fringilla urna. Duis pellentesque volutpat quam, sed ultrices arcu commodo at. Nullam accumsan vehicula sem et cursus. Vivamus dui dui, mollis sed dictum non, condimentum vel lorem. Maecenas non augue vitae odio tempus placerat nec vitae lacus. Nam tortor diam, eleifend a fermentum ac, sagittis non erat. Etiam vitae egestas tortor, finibus rutrum lectus. Ut eget est nulla. Aliquam malesuada magna ut vulputate cursus. Aenean pretium volutpat massa, ac condimentum ipsum blandit ac.<br /><br />Mauris nec nulla et nisl posuere fermentum nec id tortor. Nam ac sagittis ipsum. Cras metus orci, posuere a efficitur quis, vehicula gravida turpis. Donec consequat in odio eget ullamcorper. Curabitur vestibulum, diam sed fermentum interdum, urna elit rutrum felis, a hendrerit lacus arcu a ipsum. Fusce vel fringilla erat. Proin est augue, vehicula cursus quam eu, ultrices vehicula lectus. Nunc condimentum orci felis, ac convallis odio ultrices id. Pellentesque mollis mauris sit amet nunc ullamcorper facilisis. Duis velit nibh, lacinia quis ex ac, rhoncus pellentesque quam. Cras commodo ac arcu in imperdiet. Pellentesque viverra luctus tellus nec tincidunt. Aliquam fringilla finibus justo, id congue nisl fringilla ac. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla rutrum viverra est.</p>', 9, 2),
(3, 'Tin tức số 3', '2017-06-22 09:23:17', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc lobortis dapibus nulla, eget elementum erat vehicula a. Nullam maximus quam ut ex pharetra volutpat vitae auctor odio. Donec quis augue commodo, condimentum massa et, molestie odio. Maecenas erat est, sollicitudin rutrum lacus a, tincidunt vehicula velit. Suspendisse est metus, euismod quis varius vel, molestie at elit. Maecenas eros ligula, ullamcorper non est a, maximus posuere arcu. Etiam fringilla dapibus neque.<br /><br />Mauris felis nulla, vestibulum commodo lectus a, consectetur ultricies nulla. Morbi iaculis sagittis semper. Proin eget fringilla augue, eu lobortis felis. Etiam ultrices augue a metus aliquet tristique. Curabitur semper cursus scelerisque. Duis lacinia ex sapien, vitae maximus lacus volutpat ac. Vestibulum eget pretium urna. Sed sem enim, commodo et est vitae, elementum tempor nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam tristique sem vel tristique sagittis. Etiam porttitor interdum nisl, sit amet aliquam orci commodo vitae. Quisque egestas, diam eget ornare laoreet, quam nunc vestibulum erat, quis viverra urna ligula tempor diam. Donec tincidunt velit et est tincidunt scelerisque.<br /><br />Sed nec quam ut odio porttitor imperdiet. Aliquam erat volutpat. Fusce a lectus quis lorem maximus tempor vitae id elit. Vestibulum quis justo nulla. Etiam at tempor velit, tincidunt consectetur nunc. Donec erat risus, hendrerit non orci a, molestie faucibus purus. Morbi condimentum lorem mi, vitae varius enim eleifend sit amet. Sed gravida suscipit magna, vitae tristique urna bibendum at.<br /><br />Cras eu volutpat diam, et fringilla urna. Duis pellentesque volutpat quam, sed ultrices arcu commodo at. Nullam accumsan vehicula sem et cursus. Vivamus dui dui, mollis sed dictum non, condimentum vel lorem. Maecenas non augue vitae odio tempus placerat nec vitae lacus. Nam tortor diam, eleifend a fermentum ac, sagittis non erat. Etiam vitae egestas tortor, finibus rutrum lectus. Ut eget est nulla. Aliquam malesuada magna ut vulputate cursus. Aenean pretium volutpat massa, ac condimentum ipsum blandit ac.<br /><br />Mauris nec nulla et nisl posuere fermentum nec id tortor. Nam ac sagittis ipsum. Cras metus orci, posuere a efficitur quis, vehicula gravida turpis. Donec consequat in odio eget ullamcorper. Curabitur vestibulum, diam sed fermentum interdum, urna elit rutrum felis, a hendrerit lacus arcu a ipsum. Fusce vel fringilla erat. Proin est augue, vehicula cursus quam eu, ultrices vehicula lectus. Nunc condimentum orci felis, ac convallis odio ultrices id. Pellentesque mollis mauris sit amet nunc ullamcorper facilisis. Duis velit nibh, lacinia quis ex ac, rhoncus pellentesque quam. Cras commodo ac arcu in imperdiet. Pellentesque viverra luctus tellus nec tincidunt. Aliquam fringilla finibus justo, id congue nisl fringilla ac. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla rutrum viverra est.</p>', 10, 2);

-- --------------------------------------------------------

--
-- Stand-in structure for view `news_view`
--
CREATE TABLE `news_view` (
`ID` int(11)
,`Title` varchar(45)
,`AddedDate` timestamp
,`Path` varchar(255)
,`Status` int(1)
);

-- --------------------------------------------------------

--
-- Table structure for table `other`
--

CREATE TABLE `other` (
  `Type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `Content` mediumtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `other`
--

INSERT INTO `other` (`Type`, `Content`) VALUES
('banner', '<p><img class="img-responsive" src="https://mail.google.com/mail/u/0/?ui=2&amp;ik=2d112a45f3&amp;view=fimg&amp;th=15ce77a313ede05b&amp;attid=0.1&amp;disp=inline&amp;realattid=f_j4ezd9w00&amp;safe=1&amp;attbid=ANGjdJ8oVAxJlcUUUzdLNsdngOLDwdsglx4RQ7G0X8rlDwls8aPtSW4aBypWmFn4o_KVxAEMRtjypZrillC3SpEjTTQTynKwqY6D25icDALljHZ2BWgfMNHWZpj7a5M&amp;ats=1498532171098&amp;rm=15ce77a313ede05b&amp;zw&amp;sz=w1332-h637" alt="Banner" width="300" height="300" /></p>'),
('contact', ''),
('footer', 'C&ocirc;ng ty TNHH Ph&acirc;n Phối Việt<br />0937761215<br />MST 01234567'),
('introduction', '<pre>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc lobortis dapibus nulla, eget elementum erat vehicula a. Nullam maximus quam ut ex pharetra volutpat vitae auctor odio. Donec quis augue commodo, condimentum massa et, molestie odio. Maecenas erat est, sollicitudin rutrum lacus a, tincidunt vehicula velit. Suspendisse est metus, euismod quis varius vel, molestie at elit. Maecenas eros ligula, ullamcorper non est a, maximus posuere arcu. Etiam fringilla dapibus neque.<br /><br />Mauris felis nulla, vestibulum commodo lectus a, consectetur ultricies nulla. Morbi iaculis sagittis semper. Proin eget fringilla augue, eu lobortis felis. Etiam ultrices augue a metus aliquet tristique. Curabitur semper cursus scelerisque. Duis lacinia ex sapien, vitae maximus lacus volutpat ac. Vestibulum eget pretium urna. Sed sem enim, commodo et est vitae, elementum tempor nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam tristique sem vel tristique sagittis. Etiam porttitor interdum nisl, sit amet aliquam orci commodo vitae. Quisque egestas, diam eget ornare laoreet, quam nunc vestibulum erat, quis viverra urna ligula tempor diam. Donec tincidunt velit et est tincidunt scelerisque.<br /><br />Sed nec quam ut odio porttitor imperdiet. Aliquam erat volutpat. Fusce a lectus quis lorem maximus tempor vitae id elit. Vestibulum quis justo nulla. Etiam at tempor velit, tincidunt consectetur nunc. Donec erat risus, hendrerit non orci a, molestie faucibus purus. Morbi condimentum lorem mi, vitae varius enim eleifend sit amet. Sed gravida suscipit magna, vitae tristique urna bibendum at.<br /><br />Cras eu volutpat diam, et fringilla urna. Duis pellentesque volutpat quam, sed ultrices arcu commodo at. Nullam accumsan vehicula sem et cursus. Vivamus dui dui, mollis sed dictum non, condimentum vel lorem. Maecenas non augue vitae odio tempus placerat nec vitae lacus. Nam tortor diam, eleifend a fermentum ac, sagittis non erat. Etiam vitae egestas tortor, finibus rutrum lectus. Ut eget est nulla. Aliquam malesuada magna ut vulputate cursus. Aenean pretium volutpat massa, ac condimentum ipsum blandit ac.<br /><br />Mauris nec nulla et nisl posuere fermentum nec id tortor. Nam ac sagittis ipsum. Cras metus orci, posuere a efficitur quis, vehicula gravida turpis. Donec consequat in odio eget ullamcorper. Curabitur vestibulum, diam sed fermentum interdum, urna elit rutrum felis, a hendrerit lacus arcu a ipsum. Fusce vel fringilla erat. Proin est augue, vehicula cursus quam eu, ultrices vehicula lectus. Nunc condimentum orci felis, ac convallis odio ultrices id. Pellentesque mollis mauris sit amet nunc ullamcorper facilisis. Duis velit nibh, lacinia quis ex ac, rhoncus pellentesque quam. Cras commodo ac arcu in imperdiet. Pellentesque viverra luctus tellus nec tincidunt. Aliquam fringilla finibus justo, id congue nisl fringilla ac. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla rutrum viverra est.</pre>'),
('partner', '<pre>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc lobortis dapibus nulla, eget elementum erat vehicula a. Nullam maximus quam ut ex pharetra volutpat vitae auctor odio. Donec quis augue commodo, condimentum massa et, molestie odio. Maecenas erat est, sollicitudin rutrum lacus a, tincidunt vehicula velit. Suspendisse est metus, euismod quis varius vel, molestie at elit. Maecenas eros ligula, ullamcorper non est a, maximus posuere arcu. Etiam fringilla dapibus neque.<br /><br />Mauris felis nulla, vestibulum commodo lectus a, consectetur ultricies nulla. Morbi iaculis sagittis semper. Proin eget fringilla augue, eu lobortis felis. Etiam ultrices augue a metus aliquet tristique. Curabitur semper cursus scelerisque. Duis lacinia ex sapien, vitae maximus lacus volutpat ac. Vestibulum eget pretium urna. Sed sem enim, commodo et est vitae, elementum tempor nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam tristique sem vel tristique sagittis. Etiam porttitor interdum nisl, sit amet aliquam orci commodo vitae. Quisque egestas, diam eget ornare laoreet, quam nunc vestibulum erat, quis viverra urna ligula tempor diam. Donec tincidunt velit et est tincidunt scelerisque.<br /><br />Sed nec quam ut odio porttitor imperdiet. Aliquam erat volutpat. Fusce a lectus quis lorem maximus tempor vitae id elit. Vestibulum quis justo nulla. Etiam at tempor velit, tincidunt consectetur nunc. Donec erat risus, hendrerit non orci a, molestie faucibus purus. Morbi condimentum lorem mi, vitae varius enim eleifend sit amet. Sed gravida suscipit magna, vitae tristique urna bibendum at.<br /><br />Cras eu volutpat diam, et fringilla urna. Duis pellentesque volutpat quam, sed ultrices arcu commodo at. Nullam accumsan vehicula sem et cursus. Vivamus dui dui, mollis sed dictum non, condimentum vel lorem. Maecenas non augue vitae odio tempus placerat nec vitae lacus. Nam tortor diam, eleifend a fermentum ac, sagittis non erat. Etiam vitae egestas tortor, finibus rutrum lectus. Ut eget est nulla. Aliquam malesuada magna ut vulputate cursus. Aenean pretium volutpat massa, ac condimentum ipsum blandit ac.<br /><br />Mauris nec nulla et nisl posuere fermentum nec id tortor. Nam ac sagittis ipsum. Cras metus orci, posuere a efficitur quis, vehicula gravida turpis. Donec consequat in odio eget ullamcorper. Curabitur vestibulum, diam sed fermentum interdum, urna elit rutrum felis, a hendrerit lacus arcu a ipsum. Fusce vel fringilla erat. Proin est augue, vehicula cursus quam eu, ultrices vehicula lectus. Nunc condimentum orci felis, ac convallis odio ultrices id. Pellentesque mollis mauris sit amet nunc ullamcorper facilisis. Duis velit nibh, lacinia quis ex ac, rhoncus pellentesque quam. Cras commodo ac arcu in imperdiet. Pellentesque viverra luctus tellus nec tincidunt. Aliquam fringilla finibus justo, id congue nisl fringilla ac. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla rutrum viverra est.</pre>'),
('recruit', '<pre>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc lobortis dapibus nulla, eget elementum erat vehicula a. Nullam maximus quam ut ex pharetra volutpat vitae auctor odio. Donec quis augue commodo, condimentum massa et, molestie odio. Maecenas erat est, sollicitudin rutrum lacus a, tincidunt vehicula velit. Suspendisse est metus, euismod quis varius vel, molestie at elit. Maecenas eros ligula, ullamcorper non est a, maximus posuere arcu. Etiam fringilla dapibus neque.<br /><br />Mauris felis nulla, vestibulum commodo lectus a, consectetur ultricies nulla. Morbi iaculis sagittis semper. Proin eget fringilla augue, eu lobortis felis. Etiam ultrices augue a metus aliquet tristique. Curabitur semper cursus scelerisque. Duis lacinia ex sapien, vitae maximus lacus volutpat ac. Vestibulum eget pretium urna. Sed sem enim, commodo et est vitae, elementum tempor nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam tristique sem vel tristique sagittis. Etiam porttitor interdum nisl, sit amet aliquam orci commodo vitae. Quisque egestas, diam eget ornare laoreet, quam nunc vestibulum erat, quis viverra urna ligula tempor diam. Donec tincidunt velit et est tincidunt scelerisque.<br /><br />Sed nec quam ut odio porttitor imperdiet. Aliquam erat volutpat. Fusce a lectus quis lorem maximus tempor vitae id elit. Vestibulum quis justo nulla. Etiam at tempor velit, tincidunt consectetur nunc. Donec erat risus, hendrerit non orci a, molestie faucibus purus. Morbi condimentum lorem mi, vitae varius enim eleifend sit amet. Sed gravida suscipit magna, vitae tristique urna bibendum at.<br /><br />Cras eu volutpat diam, et fringilla urna. Duis pellentesque volutpat quam, sed ultrices arcu commodo at. Nullam accumsan vehicula sem et cursus. Vivamus dui dui, mollis sed dictum non, condimentum vel lorem. Maecenas non augue vitae odio tempus placerat nec vitae lacus. Nam tortor diam, eleifend a fermentum ac, sagittis non erat. Etiam vitae egestas tortor, finibus rutrum lectus. Ut eget est nulla. Aliquam malesuada magna ut vulputate cursus. Aenean pretium volutpat massa, ac condimentum ipsum blandit ac.<br /><br />Mauris nec nulla et nisl posuere fermentum nec id tortor. Nam ac sagittis ipsum. Cras metus orci, posuere a efficitur quis, vehicula gravida turpis. Donec consequat in odio eget ullamcorper. Curabitur vestibulum, diam sed fermentum interdum, urna elit rutrum felis, a hendrerit lacus arcu a ipsum. Fusce vel fringilla erat. Proin est augue, vehicula cursus quam eu, ultrices vehicula lectus. Nunc condimentum orci felis, ac convallis odio ultrices id. Pellentesque mollis mauris sit amet nunc ullamcorper facilisis. Duis velit nibh, lacinia quis ex ac, rhoncus pellentesque quam. Cras commodo ac arcu in imperdiet. Pellentesque viverra luctus tellus nec tincidunt. Aliquam fringilla finibus justo, id congue nisl fringilla ac. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla rutrum viverra est.</pre>');

-- --------------------------------------------------------

--
-- Table structure for table `partner`
--

CREATE TABLE `partner` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Address` text,
  `ImageID` int(11) NOT NULL,
  `Status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `partner`
--

INSERT INTO `partner` (`ID`, `Name`, `Address`, `ImageID`, `Status`) VALUES
(1, 'Coop Mart bạn của mọi nhà', 'http://www.co-opmart.com.vn', 11, 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `partner_view`
--
CREATE TABLE `partner_view` (
`ID` int(11)
,`Name` text
,`Address` text
,`Path` varchar(255)
,`Status` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `ID` int(11) NOT NULL,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Weight` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `BrandID` int(11) NOT NULL,
  `ImageID` int(11) NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`ID`, `Name`, `Weight`, `BrandID`, `ImageID`, `Status`) VALUES
(1, 'Bánh Quế Royal Dansk Chocolate 100g', '100g', 1, 2, 1),
(2, 'Bánh quy bơ Royal Dansk Blue hộp thiếc 908g', '', 1, 3, 1),
(3, 'Bánh quy bơ Royal Dansk Blue hộp thiếc 681g', '681g', 1, 4, 2),
(4, 'Bánh quy bơ Royal Dansk Blue hộp thiếc 454g', '454 gram', 1, 5, 2),
(5, 'Bánh Quế Royal Dansk chanh-gừng hộp thiếc 250g', '250 gram', 1, 6, 2),
(6, 'Bánh quy bơ Royal Dansk chocolate trắng-dâu rừng hộp thiếc 250g', '250 gram', 1, 7, 2),
(7, 'dasdas', 'asdasdasd', 2, 14, 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `product_view`
--
CREATE TABLE `product_view` (
`ID` int(11)
,`ProductName` varchar(255)
,`Weight` varchar(45)
,`BrandName` varchar(45)
,`Path` varchar(255)
,`Status` int(1)
);

-- --------------------------------------------------------

--
-- Table structure for table `slide`
--

CREATE TABLE `slide` (
  `ID` int(11) NOT NULL,
  `ImageID` int(11) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slide`
--

INSERT INTO `slide` (`ID`, `ImageID`, `Status`) VALUES
(1, 12, 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `slide_view`
--
CREATE TABLE `slide_view` (
`ID` int(11)
,`Path` varchar(255)
,`Status` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `ID` int(11) NOT NULL,
  `Username` varchar(45) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL,
  `Password` varchar(45) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL,
  `Type` varchar(10) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL DEFAULT 'admin',
  `Status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `Username`, `Password`, `Type`, `Status`) VALUES
(1, 'bazen2412', 'paladin2412', 'admin', 1);

-- --------------------------------------------------------

--
-- Structure for view `brand_view`
--
DROP TABLE IF EXISTS `brand_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `brand_view`  AS  select `b`.`ID` AS `ID`,`b`.`Name` AS `Name`,`b`.`Origin` AS `Origin`,`i`.`Path` AS `Path` from (`brand` `b` left join `image` `i` on((`b`.`ImageID` = `i`.`ID`))) ;

-- --------------------------------------------------------

--
-- Structure for view `introduction_view`
--
DROP TABLE IF EXISTS `introduction_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `introduction_view`  AS  select `intr`.`ID` AS `ID`,`intr`.`Title` AS `Title`,`intr`.`Content` AS `Content`,`i`.`Path` AS `Path` from (`introduction` `intr` left join `image` `i` on((`intr`.`ImageID` = `i`.`ID`))) ;

-- --------------------------------------------------------

--
-- Structure for view `news_view`
--
DROP TABLE IF EXISTS `news_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `news_view`  AS  select `n`.`ID` AS `ID`,`n`.`Title` AS `Title`,`n`.`AddedDate` AS `AddedDate`,`i`.`Path` AS `Path`,`n`.`Status` AS `Status` from (`new` `n` left join `image` `i` on((`n`.`ImageID` = `i`.`ID`))) ;

-- --------------------------------------------------------

--
-- Structure for view `partner_view`
--
DROP TABLE IF EXISTS `partner_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `partner_view`  AS  select `p`.`ID` AS `ID`,`p`.`Name` AS `Name`,`p`.`Address` AS `Address`,`i`.`Path` AS `Path`,`p`.`Status` AS `Status` from (`partner` `p` left join `image` `i` on((`p`.`ImageID` = `i`.`ID`))) ;

-- --------------------------------------------------------

--
-- Structure for view `product_view`
--
DROP TABLE IF EXISTS `product_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `product_view`  AS  select `p`.`ID` AS `ID`,`p`.`Name` AS `ProductName`,`p`.`Weight` AS `Weight`,`b`.`Name` AS `BrandName`,`i`.`Path` AS `Path`,`p`.`Status` AS `Status` from ((`product` `p` left join `brand` `b` on((`p`.`BrandID` = `b`.`ID`))) left join `image` `i` on((`p`.`ImageID` = `i`.`ID`))) ;

-- --------------------------------------------------------

--
-- Structure for view `slide_view`
--
DROP TABLE IF EXISTS `slide_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `slide_view`  AS  select `s`.`ID` AS `ID`,`i`.`Path` AS `Path`,`s`.`Status` AS `Status` from (`slide` `s` left join `image` `i` on((`s`.`ImageID` = `i`.`ID`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `introduction`
--
ALTER TABLE `introduction`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `introduction_ID_uindex` (`ID`);

--
-- Indexes for table `new`
--
ALTER TABLE `new`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `other`
--
ALTER TABLE `other`
  ADD PRIMARY KEY (`Type`),
  ADD UNIQUE KEY `other_Type_uindex` (`Type`);

--
-- Indexes for table `partner`
--
ALTER TABLE `partner`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `introduction`
--
ALTER TABLE `introduction`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `new`
--
ALTER TABLE `new`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `partner`
--
ALTER TABLE `partner`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `slide`
--
ALTER TABLE `slide`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
