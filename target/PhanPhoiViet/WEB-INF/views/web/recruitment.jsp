<%--
  Created by IntelliJ IDEA.
  User: Vu Anh Nguyen Duc
  Date: 6/17/2017
  Time: 22:44
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Thông tin tuyển dụng</title>
</head>
<body>
    <%@ include file="header.jsp" %>
    <div class="container-fluid" style="margin-bottom: 60px;">
        <div class="row">
            <div class="col-lg-3 col-md-3 hidden-sm hidden-xs"></div>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style="font-size: 18px">
                ${Content}
            </div>
        </div>
    </div>
    <%@ include file="footer.jsp" %>
</body>
</html>
