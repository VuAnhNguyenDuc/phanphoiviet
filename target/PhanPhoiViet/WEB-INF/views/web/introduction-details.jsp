<%--
  Created by IntelliJ IDEA.
  User: Vu Anh Nguyen Duc
  Date: 6/26/2017
  Time: 21:47
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@ include file="header.jsp" %>
    <title>${introduction.getTitle()}</title>
    <style type="text/css">
        .news-content{
            margin-bottom: 60px !important;
        }
        .news-content img{
            margin: auto;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }
    </style>
</head>
<body>
    <div style="height: 60px"></div>

    <div class="container news-content" style="font-size: 18px; text-align: center">


        <h1>${introduction.getTitle()}</h1>
        <hr style="border-top: 1px solid black;">
        ${introduction.getContent()}
    </div>

    <div style="height: 60px"></div>

    <!-- Begin footer -->
    <%@ include file="footer.jsp" %>
    <!-- End footer -->

</body>
</html>
