<%--
  Created by IntelliJ IDEA.
  User: Vu Anh Nguyen Duc
  Date: 6/17/2017
  Time: 22:43
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Đối tác</title>
</head>
<body>
    <%@ include file="header.jsp" %>

    <div class="container-fluid text-center" style="margin-top:30px">
        <div class="row">
            <c:forEach items="${AllPartners}" var="partner">
                <a href="${partner.getAddress()}" title="${partner.getName()}" style="text-decoration: none;">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 fixed-height">
                        <div class="item image-padding">
                            <div class="img-wrapper darken">
                                <img src="${partner.getPath()}" alt="Image" class="img-responsive"/>
                            </div>
                            <div class="product-content">
                                <h4 class="header" style="text-align: center;">${partner.getName()}</h4>
                            </div>
                        </div>
                    </div>
                </a>
            </c:forEach>
        </div>
    </div>

    <div style="height: 60px"></div>

    <%@ include file="footer.jsp" %>
</body>
</html>
