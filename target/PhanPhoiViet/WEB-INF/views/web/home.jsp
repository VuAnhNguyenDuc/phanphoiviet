<%--
  Created by IntelliJ IDEA.
  User: Nguyen Duc Vu Anh
  Date: 6/17/2017
  Time: 15:58
  To change this template use File | Settings | File and Code Templates.
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <%@ include file="header.jsp" %>

    <style type="text/css">
        .image-padding img{
            max-height: 257px !important;

        }
        .carousel-control{
            color: #000 !important;
        }
        .carousel-control:focus, .carousel-control:hover{
            color: #000 !important;
        }
        .video-container {
            position: relative;
            padding-bottom: 56.25%;
            padding-top: 30px; height: 0; overflow: hidden;
        }

        .video-container iframe,
        .video-container object,
        .video-container embed {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }
    </style>
    <head><title>Trang chủ</title></head>
    <!-- Begin carousel -->
    <div class="container text-center">
        <div id="myCarousel" class="carousel slide col-lg-12 col-md-12" data-ride="carousel" style="float: none; padding: 0px; border: 1px solid black;">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active ">
                    <div class="video-container">
                        <iframe width="853" height="200"  src="https://www.youtube.com/embed/vhxpwgd9GuI" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>

                <c:forEach items="${slideImageList}" var="slideImage" varStatus="loop">

                    <div class="item">
                        <img src="${slideImage.getPath()}" alt="Image" style="display: block; margin-left: auto; margin-right: auto; "/>
                    </div>

                </c:forEach>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <!-- End carousel -->

    <!-- Begin body -->
    <div class="container text-center">
        <h3> THƯƠNG HIỆU </h3>
        <div class="row">
            <div class="large-12 columns">
                <div class="owlBrand owl-carousel owl-theme" >
                    <c:forEach items="${brandList}" var="brand">
                        <a href="/products?brandID=${brand.getID()}" style="text-decoration: none;">
                            <div class="item image-padding">
                                <div class="img-wrapper darken">
                                        <img class="img-responsive" src="${brand.getPath()}" alt="Thương hiệu"/>
                                </div>
                                <div class="product-content">
                                    <h4 class="header" style="text-align: center;">${brand.getName()}</h4>
                                </div>
                            </div>
                        </a>
                    </c:forEach>
                    </div>
                </div>
            </div>

        <h3> SẢN PHẨM NỔI BẬT </h3>
        <div class="row">
            <div class="large-12 columns">
                <div class="owlProducts owl-carousel owl-theme">
                    <c:forEach items="${HotProducts}" var="product">
                        <a href="/product-details?id=${product.getID()}"  style="text-decoration: none;">
                            <div class="item image-padding">
                                <div class="img-wrapper darken">
                                    <img src="${product.getPath()}" alt="Sản phẩm Hot" class="img-responsive"/>
                                </div>
                                <div class="product-content">
                                    <h4 class="header">${product.getProductName()}</h4>
                                </div>
                            </div>
                        </a>
                    </c:forEach>
                </div>
            </div>
        </div>

        <h3> SẢN PHẨM </h3>
        <div class="row">
            <div class="large-12 columns">
                <div class="owlProducts owl-carousel owl-theme">
                    <c:forEach items="${AllProducts}" var="product">
                        <a href="/product-details?id=${product.getID()}" style="text-decoration: none;">
                            <div class="item image-padding">
                                <div class="img-wrapper darken">
                                    <img src="${product.getPath()}" alt="Sản phẩm Hot" class="img-responsive"/>
                                </div>
                                <div class="product-content">
                                    <h4 class="header">${product.getProductName()}</h4>
                                </div>
                            </div>
                        </a>
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>

    <div class="container text-center">
        <div class="row">
            <h3> TIN TỨC </h3>
            <!-- https://stackoverflow.com/questions/26329908/owl-carousel-set-different-items-number-for-each-of-many-sliders-placed-on-the-->
            <div class="large-12 columns">
                <div class="owlNews owl-carousel owl-theme">
                    <c:forEach items="${AllNews}" var="news">
                        <a href="/news-details?id=${news.getID()}" style="text-decoration: none;">
                            <div class="item image-padding">
                                <div class="img-wrapper darken">
                                    <img src="${news.getPath()}" alt="Tin tức" class="img-responsive"/>
                                </div>
                                <div class="product-content">
                                    <h4 class="header">${news.getTitle()}</h4>
                                </div>
                            </div>
                        </a>
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>
    <!-- End body -->

    <div style="height: 60px; position: relative;"></div>

    <!-- Begin footer -->
    <%@ include file="footer.jsp" %>
    <!-- End footer -->

    <script type="application/javascript">
        $(document).ready(function() {
            var owlBrand = $(".owlBrand");
            owlBrand.owlCarousel({
                margin: 10,
                nav: true,
                loop: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                    },
                    1000: {
                        items: 4
                    }
                }
            });

            $(".owlNews").owlCarousel({
                margin: 10,
                nav: true,
                loop: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                    },
                    1000: {
                        items: 4
                    }
                }
            });

            $('.owlProducts').owlCarousel({
                margin: 10,
                nav: true,
                loop: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                    },
                    1000: {
                        items: 4
                    }
                }

            });

            $('.darken').hover(function() {
                this.style.backgroundColor = 'black';
                $(this).find('img').fadeTo(500, 0.8);
            }, function() {
                this.style.backgroundColor = 'white';
                $(this).find('img').fadeTo(500, 1);
            });
        })
    </script>
</body>
</html>