<%--
  Created by IntelliJ IDEA.
  User: Vu Anh Nguyen Duc
  Date: 6/17/2017
  Time: 22:44
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Đối tác</title>
    <style type="text/css">
        .Noti{
            background-color: #FFFAF3;
            color: #573A08;
            font-size: 14px;
            padding: 15px;
            margin-bottom: 10px;
            border: 1px solid black;
        }
        .Noti p{
            font-size: 16px;
        }
        .form-error{
            color:red !important;
            font-weight: 700 !important;
        }
    </style>
</head>
<body>
    <%@ include file="header.jsp" %>
    <div class="container-fluid" style="margin-bottom: 60px;">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="row" style="padding: 15px">
                    ${Content}
                </div>
                <div class="row" style="margin-bottom: 20px">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3918.1567476606338!2d106.65266751418324!3d10.875680060329197!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317529ddf5219545%3A0x8582b7850ec0e767!2zTMOqIFRo4buLIFJpw6puZywgVGjhu5tpIEFuLCBRdeG6rW4gMTIsIEjhu5MgQ2jDrSBNaW5oLCBWaWV0bmFt!5e0!3m2!1sen!2sin!4v1498486191202" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>

            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="font-size: 18px">
                <div class="Noti">
                    <p>Lưu ý</p>
                    <ul>
                        <li>Những trường có dấu sao là bắt buộc</li>
                    </ul>
                </div>
                <form:form modelAttribute="insertFeedbackForm" method="post">
                    <div class="form-group>">
                        <label for="Name">Họ và tên*: </label>
                        <form:input path="Name" type="text" class="form-control" id="Name"/>
                        <form:errors path="Name" cssClass="form-error" class="form-control"/>
                    </div>
                    <div class="form-group>">
                        <label for="Email">Email: </label>
                        <form:input path="Email" type="text" class="form-control" id="Email"/>
                        <form:errors path="Email" cssClass="form-error" class="form-control"/>
                    </div>
                    <div class="form-group>">
                        <label for="Phone">Điện thoại: </label>
                        <form:input path="Phone" type="text" class="form-control" id="Phone"/>
                        <form:errors path="Phone" cssClass="form-error" class="form-control"/>
                    </div>
                    <div class="form-group>">
                        <label for="Content">Nội dung*: </label>
                        <form:textarea path="Content" type="text" class="form-control" id="Content" rows="10"/>
                        <form:errors path="Content" cssClass="form-error" class="form-control"/>
                    </div>
                    <div class="text-center" style="margin-top : 60px; margin-bottom: 60px;">
                        <button type="submit" class="btn btn-primary">
                            GỬI
                        </button>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
    <%@ include file="footer.jsp" %>
</body>
</html>
