<%--
  Created by IntelliJ IDEA.
  User: Nguyen Duc Vu Anh
  Date: 6/17/2017
  Time: 16:01
  To change this template use File | Settings | File and Code Templates.
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<spring:url value="resources/js/owl.carousel.js" var="owlCarouselJs" />
<spring:url value="resources/js/jquery.elevatezoom.js" var="elevateZoomJs" />
<!-- Begin footer -->
<footer class="footer" style="position: relative;">
    <div class="row center" style="text-align: center; ">
        ${Footer}
    </div>
</footer>


<script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="application/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="application/javascript" src="${owlCarouselJs}"></script>
<script type="application/javascript" src="${elevateZoomJs}"></script>
<script type="application/javascript" src="http://thdoan.github.io/magnify/js/jquery.magnify.js"></script>
<script type="application/javascript">
    $('#myCarousel').carousel({
        //interval: 2000,
        interval : false,
        cycle: true
    });
    $('.darken').hover(function() {
        this.style.backgroundColor = 'black';
        $(this).find('img').fadeTo(500, 0.8);
    }, function() {
        this.style.backgroundColor = 'white';
        $(this).find('img').fadeTo(500, 1);
    });
</script>
<!-- Optional mobile plugin (uncomment the line below to enable): -->
<!-- <script src="/js/jquery.magnify-mobile.js"></script> -->
<!-- End footer -->