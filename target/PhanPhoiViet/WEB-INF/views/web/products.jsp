<%@ page import="com.hibernate.ProductHibernate" %>
<%@ page import="com.model.BrandView" %>
<%@ page import="java.util.List" %>
<%@ page import="com.model.ProductView" %><%--
  Created by IntelliJ IDEA.
  User: Vu Anh Nguyen Duc
  Date: 6/17/2017
  Time: 22:43
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
    ProductHibernate productHibernate = new ProductHibernate();
%>

<html>
    <%@ include file="header.jsp" %>
    <head><title>Sản phẩm</title></head>

    <!-- Begin body -->
    <div class="container-fluid text-center" style="margin-top:30px">
        <c:forEach items="${brandList}" var="brand">
            <h3> Thương hiệu ${brand.getName()} </h3>
            <div class="row">
                <%
                    BrandView brandView = (BrandView)pageContext.getAttribute("brand");
                    int brandID = brandView.getID();
                    List<ProductView> products = productHibernate.getProductsByBrand(brandID);
                    session.setAttribute("products",products);
                %>
                <div class="large-12 columns">
                    <div class="owlProducts owl-carousel owl-theme">
                        <c:forEach items="${products}" var="product">
                            <a href="/product-details?id=${product.getID()}" style="text-decoration: none;">
                                <div class="item image-padding">
                                    <div class="img-wrapper darken">
                                        <img class="img-responsive" src="${product.getPath()}" alt="Image"/>
                                    </div>
                                    <div class="product-content">
                                        <h4 class="header">${product.getProductName()}</h4>
                                    </div>
                                </div>
                            </a>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
    <!-- End body -->

    <div style="height: 60px"></div>

    <!-- Begin footer -->
    <%@ include file="footer.jsp" %>
    <!-- End footer -->

    <script type="application/javascript">
        $(document).ready(function() {
            $('.owlProducts').owlCarousel({
                margin: 10,
                nav: true,
                loop: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                    },
                    1000: {
                        items: 4
                    }
                }
            });

            $('.darken').hover(function() {
                this.style.backgroundColor = 'black';
                $(this).find('img').fadeTo(500, 0.8);
            }, function() {
                this.style.backgroundColor = 'white';
                $(this).find('img').fadeTo(500, 1);
            });
        })
    </script>
</body>
</html>
