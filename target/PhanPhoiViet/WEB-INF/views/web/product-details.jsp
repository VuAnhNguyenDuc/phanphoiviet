<%--
  Created by IntelliJ IDEA.
  User: Vu Anh Nguyen Duc
  Date: 6/17/2017
  Time: 22:42
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <%@ include file="header.jsp" %>
    <head>
        <title>Sản phẩm chi tiết</title>
        <style type="text/css">
            .product-content{
                margin-bottom: 60px !important;
                position:relative;
            }

            @media (min-width: 769px) {
                .vertical-center {
                    min-height: 100%;  /* Fallback for vh unit */
                    min-height: 100vh; /* You might also want to use
                        'height' property instead.

                        Note that for percentage values of
                        'height' or 'min-height' properties,
                        the 'height' of the parent element
                        should be specified explicitly.

                        In this case the parent of '.vertical-center'
                        is the <body> element */

                    /* Make it a flex container */
                    display: -webkit-box;
                    display: -moz-box;
                    display: -ms-flexbox;
                    display: -webkit-flex;
                    display: flex;

                    /* Align the bootstrap's container vertically */
                    -webkit-box-align : center;
                    -webkit-align-items : center;
                    -moz-box-align : center;
                    -ms-flex-align : center;
                    align-items : center;

                    /* In legacy web browsers such as Firefox 9
                       we need to specify the width of the flex container */
                    width: 100%;

                    /* Also 'margin: 0 auto' doesn't have any effect on flex items in such web browsers
                       hence the bootstrap's container won't be aligned to the center anymore.

                       Therefore, we should use the following declarations to get it centered again */
                    -webkit-box-pack : center;
                    -moz-box-pack : center;
                    -ms-flex-pack : center;
                    -webkit-justify-content: center;
                    justify-content: center;
                }

                .product-info{
                    font-size: 24px;
                }
            }

            .product-attribute{
                font-weight: bold;
            }


        </style>
    </head>

    <div class="container-fluid product-content">
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <img src="${Product.getPath()}" class="img-responsive" alt="productImg">
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 product-info">
                    <div class="table-responsive" style="width:100%; height: 100%;">
                        <table class="table product-info" >
                            <col width="80">
                            <col width="130">
                            <thead>
                            <tr>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="product-attribute">Tên sản phẩm</td>
                                <td>${Product.getProductName()}</td>
                            </tr>
                            <tr>
                                <td class="product-attribute">Khối lượng</td>
                                <td>${Product.getWeight()}</td>
                            </tr>
                            <tr>
                                <td class="product-attribute">Nhà sản xuất</td>
                                <td>${Product.getBrandName()}</td>
                            </tr>
                            <tr>
                                <td class="product-attribute">Xuất xứ</td>
                                <td>${Origin}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">

        </div>
    </div>

    <div style="height: 60px"></div>

    <!-- Begin footer -->
    <%@ include file="footer.jsp" %>
    <!-- End footer -->
</body>
</html>
