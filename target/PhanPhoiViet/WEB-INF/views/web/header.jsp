<%--
  Created by IntelliJ IDEA.
  User: Nguyen Duc Vu Anh
  Date: 6/17/2017
  Time: 16:01
  To change this template use File | Settings | File and Code Templates.
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<spring:url value="resources/css/style.css" var="generalCss" />
<spring:url value="resources/css/owl.carousel.min.css" var="owlCarouselCss1" />
<spring:url value="resources/css/owl.theme.default.min.css" var="owlCarouselCss2" />
<spring:url value="resources/css/custom_zoom.css" var="zoomCss" />

<%
    String uri = request.getRequestURI();
    String pageName = uri.substring(uri.lastIndexOf("/")+1);
%>
<head lang="vi">
    <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="${owlCarouselCss1}" rel="stylesheet" media="screen">
    <link href="${owlCarouselCss2}" rel="stylesheet" media="screen">
    <link href="${generalCss}" rel="stylesheet" media="screen">
    <link href="${zoomCss}" rel="stylesheet" media="screen">
    <style type="text/css">
        .banner{
            position: relative;
        }
        .banner img{
            margin: auto;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }
    </style>
    <link rel="stylesheet prefetch" href="http://thdoan.github.io/magnify/css/magnify.css">

</head>


<body>

    <!-- Begin Header -->
    <nav class="navbar navbar-default" style="background-color: white;">
        <div class="row col-md-10 col-lg-10 col-sm-12 col-xs-12 center banner">
            <a href="/home">
                ${Banner}
            </a>
        </div>
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="pull-left navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!--
                <a href="${contextPath}/home" class="visible-xs" style="text-decoration: none; font-size : 20px; font-weight: bold; color: black; margin-top: 20px; margin-left: 20px">PHÂN PHỐI VIỆT</a>-->
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class ="nav navbar-nav" id="header-nav-list">
                    <li <% if(pageName.equals("home.jsp") || pageName.equals("")){  %> class="menu-selected" <% }%>>
                        <a href="${contextPath}/home">Trang chủ</a>
                    </li>
                    <li <% if(pageName.equals("introduction.jsp")){  %> class="menu-selected" <% }%> >
                        <a href="${contextPath}/introduction">Giới thiệu</a>
                    </li>

                    <% if(pageName.equals("products.jsp") || pageName.equals("product-details.jsp")){  %>
                        <li class="dropdown dropdown-large menu-selected" >
                            <!-- https://bootsnipp.com/snippets/featured/large-dropdown-menu -->
                            <a href="${contextPath}/products" class="menu-selected">Sản phẩm</a>
                        </li>
                    <% }else{%>
                        <li class="dropdown dropdown-large" >
                            <a href="${contextPath}/products">Sản phẩm</a>
                        </li>
                    <%}%>



                    <li <% if(pageName.equals("partners.jsp")){  %> class="menu-selected" <% }%>>
                        <a href="${contextPath}/partners">Đối tác</a>
                    </li>
                    <li <% if(pageName.equals("news.jsp") || pageName.equals("news-details.jsp")){  %> class="menu-selected" <% }%>>
                        <a href="${contextPath}/news">Tin tức</a>
                    </li>
                    <!--
                    <li <% if(pageName.equals("recruitment.jsp")){  %> class="menu-selected" <% }%>>
                        <a href="${contextPath}/recruitment">Tuyển dụng</a>
                    </li>
                    -->
                    <li <% if(pageName.equals("contact.jsp")){  %> class="menu-selected" <% }%>>
                        <a href="${contextPath}/contact">Liên hệ</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Header -->

