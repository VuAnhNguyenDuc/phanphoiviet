<%--
  Created by IntelliJ IDEA.
  User: Vu Anh Nguyen Duc
  Date: 6/23/2017
  Time: 10:35
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>SLIDE SHOW</title>
</head>
<body>
<p><a href="/admin/slideImage/insert" class="btn btn-primary">THÊM HÌNH MỚI</a></p>
<div class="table-responsive" style="width: 100%;">
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Mã</th>
            <th class="hidden-xs">Ảnh đại diện</th>
            <th>Trạng thái</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${slideImageList}" var="slideImage">
            <tr>
                <td>${slideImage.getID()}</td>
                <td class="item-img hidden-xs"><img src="${slideImage.getPath()}" class="img-responsive zoom" alt="brandImg" data-magnify-src="${slideImage.getPath()}"></td>
                <td>
                    <c:if test="${slideImage.getStatus() == 1}">
                        Hoạt động
                    </c:if>
                    <c:if test="${slideImage.getStatus() == -1}">
                        Vô hiệu
                    </c:if>
                </td>
                <td>
                    <a href="/admin/slideImage/update?id=${slideImage.getID()}" class="btn btn-primary">Chỉnh sửa</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>
