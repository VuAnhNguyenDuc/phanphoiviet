<%--
  Created by IntelliJ IDEA.
  User: Vu Anh Nguyen Duc
  Date: 6/18/2017
  Time: 13:26
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>SẢN PHẨM</title>
</head>
<body>
    <p><a href="/admin/product/insert" class="btn btn-primary">THÊM SẢN PHẨM MỚI</a></p>
    <div class="table-responsive" style="width: 100%;">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Mã</th>
                    <th>Tên sản phẩm</th>
                    <th>Khối lượng</th>
                    <th>Thương hiệu</th>
                    <th class="hidden-xs">Hình ảnh</th>
                    <th>Trạng thái</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${productList}" var="product">
                    <tr>
                        <td>${product.getID()}</td>
                        <td>${product.getProductName()}</td>
                        <td>${product.getWeight()}</td>
                        <td>${product.getBrandName()}</td>
                        <td class="item-img hidden-xs"><img src="${product.getPath()}" class="img-responsive zoom" alt="brandImg" data-magnify-src="${product.getPath()}"></td>
                        <td>
                            <c:if test="${product.getStatus() == 1}">
                                Hoạt động
                            </c:if>
                            <c:if test="${product.getStatus() == -1}">
                                Vô hiệu
                            </c:if>
                            <c:if test="${product.getStatus() == 2}">
                                Nổi bật
                            </c:if>
                        </td>
                        <td>
                            <a href="/admin/product/update?id=${product.getID()}" class="btn btn-primary">Chỉnh sửa</a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>

</body>
</html>
