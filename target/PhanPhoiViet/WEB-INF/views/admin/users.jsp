<%--
  Created by IntelliJ IDEA.
  User: Vu Anh Nguyen Duc
  Date: 6/18/2017
  Time: 13:29
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>NGƯỜI DÙNG</title>
</head>
<body>
<p><a href="/admin/users/insert" class="btn btn-primary">THÊM NGƯỜI DÙNG MỚI</a></p>
<div class="table-responsive" style="width: 100%;">
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Mã</th>
            <th>Tên đăng nhập</th>
            <th>Mật mã</th>
            <th>Kiểu người dùng</th>
            <th>Trạng thái</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            <c:forEach items="${userList}" var="user">
                <tr>
                    <td>${user.getID()}</td>
                    <td>${user.getUsername()}</td>
                    <td>${user.getPassword()}</td>
                    <td>
                        <c:if test="${user.getType() == 'admin'}">
                            Quản trị viên
                        </c:if>
                        <c:if test="${user.getType() == 'user'}">
                            Người dùng
                        </c:if>
                    </td>
                    <td>
                        <c:if test="${user.getStatus() == 1}">
                            Hoạt động
                        </c:if>
                        <c:if test="${user.getStatus() == -1}">
                            Vô hiệu
                        </c:if>
                    </td>
                    <td>
                        <a href="/admin/users/update?id=${user.getID()}" class="btn btn-primary">Chỉnh sửa</a>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>

</body>
</html>
