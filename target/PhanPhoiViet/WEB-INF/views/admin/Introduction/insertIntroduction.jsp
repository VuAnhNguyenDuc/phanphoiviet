<%--
  Created by IntelliJ IDEA.
  User: Vu Anh Nguyen Duc
  Date: 6/26/2017
  Time: 21:28
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<spring:url value="css/style.css" var="generalCss" />
<spring:url value="css/content.min.css" var="TinyMCE_Css" />
<spring:url value="js/general.js" var="generalJs" />
<spring:url value="js/tinymce/tinymce.min.js" var="TinyMCE_Js" />
<html>
<head>
    <title>THÊM GIỚI THIỆU</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="${generalCss}" rel="stylesheet" media="screen">

    <script type="application/javascript" src="${generalJs}"></script>
    <script type="application/javascript" src="${TinyMCE_Js}"></script>

    <script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="application/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row" style="height: 15vh;">

    </div>
    <form:form modelAttribute="insertEditIntroductionForm" method="post" enctype="multipart/form-data">
        <div class="form-group>">
            <label for="title">Tiêu đề: </label>
            <form:input path="Title" type="text" class="form-control" id="title"/>
            <form:errors path="Title" cssClass="form-error" class="form-control"/>
        </div>
        <div class="form-group>">
            <label for="editor">Nội dung: </label>
            <form:textarea cssClass="form-control" path="Content" id='editor' name="editor"/>
            <form:errors path="Content" cssClass="form-error" class="form-control"/>
            <script type="application/javascript">
                tinyMCE.init({
                    selector: "textarea",
                    height: 500,
                    menubar: false,
                    plugins: [
                        'advlist autolink lists link image charmap print preview anchor',
                        'searchreplace visualblocks code fullscreen',
                        'insertdatetime media table contextmenu paste code'
                    ],
                    toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                    content_css: [
                        '${generalCss}', '${TinyMCE_Css}'],
                    image_class_list: [{title: 'Responsive', value: 'img-responsive'}]
                });
            </script>
        </div>
        <div class="form-group>">
            <label for="file">Ảnh đại diện: </label>
            <form:input path="Path" type="file" class="form-control" id="file" accept="image/gif, image/jpeg, image/png" onchange="readURL(this)"/>
            <c:if test="${not empty Error}">
                <label class="form-error">${Error}</label>
            </c:if>
        </div>
        <div class="form-group">
            <img id="imagePreview" src="#" class="img-responsive" alt="Bạn chưa chọn ảnh nào" style="max-height: 500px; max-width: 500px">
        </div>
        <div class="text-center" style="margin-top : 60px; margin-bottom: 60px;">
            <button type="submit" class="btn btn-primary">
                THÊM
            </button>
        </div>
    </form:form>
</div>

</body>
</html>
