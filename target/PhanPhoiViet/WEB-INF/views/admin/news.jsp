<%--
  Created by IntelliJ IDEA.
  User: Vu Anh Nguyen Duc
  Date: 6/18/2017
  Time: 13:28
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>TIN TỨC</title>
</head>
<body>
    <p><a href="/admin/news/insert" class="btn btn-primary">THÊM TIN TỨC MỚI</a></p>
    <div class="table-responsive" style="width: 100%;">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Mã</th>
                <th>Tiêu đề</th>
                <th>Ngày thêm</th>
                <th>Nội dung</th>
                <th class="hidden-xs">Ảnh đại diện</th>
                <th>Trạng thái</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                <c:forEach items="${newsList}" var="news">
                    <tr>
                        <td>${news.getID()}</td>
                        <td>${news.getTitle()}</td>
                        <td>${news.getAddedDate()}</td>
                        <td>
                            <a href="/admin/news/content?id=${news.getID()}" class="btn btn-primary">Chi tiết...</a>
                        </td>
                        <td class="item-img hidden-xs"><img src="${news.getPath()}" class="img-responsive zoom" alt="brandImg" data-magnify-src="${news.getPath()}"></td>
                        <td>
                            <c:if test="${news.getStatus() == 1}">
                                Hoạt động
                            </c:if>
                            <c:if test="${news.getStatus() == -1}">
                                Vô hiệu
                            </c:if>
                            <c:if test="${news.getStatus() == 2}">
                                Nổi bật
                            </c:if>
                        </td>
                        <td>
                            <a href="/admin/news/update?id=${news.getID()}" class="btn btn-primary">Chỉnh sửa</a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</body>
</html>
