<%--
  Created by IntelliJ IDEA.
  User: Vu Anh Nguyen Duc
  Date: 6/18/2017
  Time: 13:29
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>THƯƠNG HIỆU</title>
</head>
<body>
<p><a href="/admin/brand/insert" class="btn btn-primary">THÊM THƯƠNG HIỆU MỚI</a></p>
<div class="table-responsive" style="width: 100%;">
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Mã</th>
            <th>Tên thương hiệu</th>
            <th>Xuất xứ</th>
            <th class="hidden-xs">Hình ảnh</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            <c:forEach items="${brandList}" var="brand">
                <tr>
                    <td>${brand.getID()}</td>
                    <td>${brand.getName()}</td>
                    <td>${brand.getOrigin()}</td>
                    <td class="item-img hidden-xs"><img src="${brand.getPath()}" class="img-responsive zoom" alt="brandImg" data-magnify-src="${brand.getPath()}"/></td>
                    <td>
                        <a href="/admin/brand/update?id=${brand.getID()}" class="btn btn-primary">Chỉnh sửa</a>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>

</body>
</html>
