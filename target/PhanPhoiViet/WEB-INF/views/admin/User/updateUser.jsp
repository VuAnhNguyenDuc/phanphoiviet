<%@ page import="com.form.InsertEditUserForm" %><%--
  Created by IntelliJ IDEA.
  User: Vu Anh Nguyen Duc
  Date: 6/20/2017
  Time: 23:20
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
    InsertEditUserForm insertEditUserForm = (InsertEditUserForm) request.getAttribute("insertEditUserForm");
    String Type = insertEditUserForm.getType();
    int Status = insertEditUserForm.getStatus();
%>
<spring:url value="css/style.css" var="generalCss" />
<spring:url value="css/content.min.css" var="TinyMCE_Css" />
<spring:url value="js/general.js" var="generalJs" />
<spring:url value="js/tinymce/tinymce.min.js" var="TinyMCE_Js" />
<html>
<head>
    <title>CHỈNH SỬA NGƯỜI DÙNG</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="${generalCss}" rel="stylesheet" media="screen">

    <script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="application/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row" style="height: 15vh;"></div>
    <form:form modelAttribute="insertEditUserForm" method="post">
        <div class="form-group>">
            <label for="name">Tên đăng nhập: </label>
            <form:input path="Username" type="text" class="form-control" id="name" value="${insertEditUserForm.getUsername()}"/>
            <form:errors path="Username" cssClass="form-error" class="form-control"/>
        </div>
        <div class="form-group>">
            <label for="Password">Mật mã: </label>
            <form:input path="Password" type="text" class="form-control" id="Password" value="${insertEditUserForm.getPassword()}"/>
            <form:errors path="Password" cssClass="form-error" class="form-control"/>
        </div>
        <div class="form-group>">
            <label for="userType">Kiểu người dùng: </label>
            <form:select path="Type" class="form-control" id="userType">
                <% if(Type.equals("admin")){%>
                    <form:option value="admin">Quản trị viên</form:option>
                    <form:option value="user">Người dùng</form:option>
                <% } %>
                <% if(Type.equals("user")){%>
                <form:option value="admin">Quản trị viên</form:option>
                <form:option value="user" selected="true">Người dùng</form:option>
                <% } %>
            </form:select>
        </div>
        <div class="form-group>">
            <label for="Status">Trạng thái: </label>
            <form:select path="Status" class="form-control" id="userType">
                <% if(Status == 1){%>
                    <form:option value="1" selected="true">Hoạt động</form:option>
                    <form:option value="-1">Vô hiệu</form:option>
                <%}%>
                <% if(Status == -1){%>
                    <form:option value="1">Hoạt động</form:option>
                    <form:option value="-1" selected="true">Vô hiệu</form:option>
                <%}%>
            </form:select>
            <form:errors path="Status" cssClass="form-error" class="form-control"/>
        </div>
        <div class="text-center" style="margin-top : 60px; margin-bottom: 60px;">
            <button type="submit" class="btn btn-primary">
                SỬA
            </button>
        </div>
    </form:form>
</div>

</body>
</html>
