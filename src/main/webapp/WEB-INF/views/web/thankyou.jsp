<%--
  Created by IntelliJ IDEA.
  User: Vu Anh Nguyen Duc
  Date: 6/23/2017
  Time: 04:47
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Cảm ơn quý khách</title>
</head>
<body>
<%@ include file="header.jsp" %>
<div class="container" style="text-align: center; font-size: 18px; font-weight: bold;">
    <p>CẢM ƠN QUÝ KHÁCH ĐÃ GÓP Ý CHO CHÚNG TÔI, CHÚNG TÔI SẼ XỬ LÝ THÔNG TIN NÀY SỚM NHẤT CÓ THỂ</p>
    <p><a href="/home">Bấm vào đây để quay lại trang chủ</a> </p>
</div>

<div style="height: 60px"></div>

<%@ include file="footer.jsp" %>
</body>
</html>
