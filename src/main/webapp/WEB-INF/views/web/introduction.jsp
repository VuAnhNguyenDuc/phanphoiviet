<%--
  Created by IntelliJ IDEA.
  User: Vu Anh Nguyen Duc
  Date: 6/17/2017
  Time: 22:43
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ include file="header.jsp" %>
<head><title>Giới thiệu</title></head>

<!-- Begin body -->
<div class="container-fluid text-center" style="margin-top:30px">
    <div class="row">
        <c:forEach items="${AllIntroduction}" var="introduction">
            <a href="/introduction-details?id=${introduction.getID()}" title="${introduction.getTitle()}" style="text-decoration: none;">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 fixed-height">
                    <div class="item image-padding">
                        <div class="img-wrapper darken">
                            <img src="${introduction.getPath()}" alt="Image" class="img-responsive"/>
                        </div>
                        <div class="product-content">
                            <h3 class="header">${introduction.getTitle()}</h3>
                        </div>
                    </div>
                </div>
            </a>
        </c:forEach>
    </div>
</div>
<!-- End body -->

<div style="height: 60px"></div>

<!-- Begin footer -->
<%@ include file="footer.jsp" %>
<!-- End footer -->

<script type="application/javascript">
    $(document).ready(function() {
        $('.darken').hover(function() {
            this.style.backgroundColor = 'black';
            $(this).find('img').fadeTo(500, 0.8);
        }, function() {
            this.style.backgroundColor = 'white';
            $(this).find('img').fadeTo(500, 1);
        });
    })
</script>
</body>
</html>
