<%--
  Created by IntelliJ IDEA.
  User: Vu Anh Nguyen Duc
  Date: 6/20/2017
  Time: 23:20
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<spring:url value="css/style.css" var="generalCss" />
<spring:url value="css/content.min.css" var="TinyMCE_Css" />
<spring:url value="js/general.js" var="generalJs" />
<spring:url value="js/tinymce/tinymce.min.js" var="TinyMCE_Js" />
<html>
<head>
    <title>CHỈNH SỬA THƯƠNG HIỆU</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="${generalCss}" rel="stylesheet" media="screen">

    <script type="application/javascript" src="${generalJs}"></script>
    <script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="application/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row" style="height: 15vh;"></div>
    <form:form modelAttribute="insertEditBrandForm" method="post" enctype="multipart/form-data">

        <div class="form-group>">
            <label for="name">Tên thương hiệu: </label>
            <form:input path="Name" type="text" class="form-control" id="name" value="${insertEditBrandForm.getName()}" />
            <form:errors path="Name" cssClass="form-error" class="form-control"/>
        </div>
        <div class="form-group>">
            <label for="Origin">Xuất xứ: </label>
            <form:input path="Origin" type="text" class="form-control" id="Origin" value="${insertEditBrandForm.getOrigin()}"/>
            <form:errors path="Origin" cssClass="form-error" class="form-control"/>
        </div>
        <div class="form-group>">
            <label>Hình ảnh: </label>
            <form:input path="Path" type="file" class="form-control" id="file" name="file" accept="image/gif, image/jpeg, image/png" onchange="readURL(this)"/>
            <c:if test="${not empty Error}">
                <label class="form-error">${Error}</label>
            </c:if>
        </div>
        <div class="form-group">
            <img id="imagePreview" src="${ImageLink}" class="img-responsive" alt="Bạn chưa chọn ảnh nào" style="max-height: 500px; max-width: 500px">
        </div>
        <div class="text-center" style="margin-top : 60px; margin-bottom: 60px;">
            <button type="submit" class="btn btn-primary">
                SỬA
            </button>
        </div>

    </form:form>
</div>

</body>
</html>
