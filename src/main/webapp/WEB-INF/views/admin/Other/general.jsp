<%--
  Created by IntelliJ IDEA.
  User: Vu Anh Nguyen Duc
  Date: 6/21/2017
  Time: 02:13
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
    String pageName = request.getAttribute("pageName").toString();
%>
<spring:url value="css/style.css" var="generalCss" />
<spring:url value="css/content.min.css" var="TinyMCE_Css" />
<spring:url value="js/general.js" var="generalJs" />
<spring:url value="js/tinymce/tinymce.min.js" var="TinyMCE_Js" />
<html>
<head lang="vi">
    <% if(pageName.equals("banner.jsp")){ %>
        <title>BANNER</title>
    <%}%>
    <% if(pageName.equals("introduction.jsp")){ %>
    <title>GIỚI THIỆU</title>
    <%}%>
    <% if(pageName.equals("partner.jsp")){ %>
    <title>ĐỐI TÁC</title>
    <%}%>
    <% if(pageName.equals("contact.jsp")){ %>
    <title>LIÊN HỆ</title>
    <%}%>
    <% if(pageName.equals("recruit.jsp")){ %>
    <title>TUYỂN DỤNG</title>
    <%}%>
    <% if(pageName.equals("footer.jsp")){ %>
    <title>FOOTER</title>
    <%}%>

    <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="${generalCss}" rel="stylesheet" media="screen">

    <script type="application/javascript" src="${generalJs}"></script>
    <script type="application/javascript" src="${TinyMCE_Js}"></script>

    <script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="application/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <style type="text/css">
        body{
            font-weight: bold;
        }
        /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
        .row.content {height: 550px}

        /* Set gray background color and 100% height */
        .sidenav {
            background-color: #f1f1f1;
            height: 100%;
        }

        .item-img img{
            width: 200px !important;
            height: 200px !important;
        }

        /* On small screens, set height to 'auto' for the grid */
        @media screen and (max-width: 767px) {
            .row.content {height: auto;}
        }
    </style>
</head>
<body>

<nav class="navbar navbar-inverse visible-xs">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/admin">PHÂN PHỐI VIỆT</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li <% if(pageName.equals("product.jsp")){ %> class="active" <%}%>><a href="/admin/product">SẢN PHẨM</a></li>
                <li <% if(pageName.equals("brand.jsp")){ %> class="active" <%}%>><a href="/admin/brand">THƯƠNG HIỆU</a></li>
                <li <% if(pageName.equals("news.jsp")){ %> class="active" <%}%>><a href="/admin/news">TIN TỨC</a></li>
                <li <% if(pageName.equals("introduction.jsp")){ %> class="active" <%}%>><a href="/admin/introduction">GIỚI THIỆU</a></li>
                <li <% if(pageName.equals("users.jsp")){ %> class="active" <%}%>><a href="/admin/users">NGƯỜI DÙNG</a></li>
                <li <% if(pageName.equals("banner.jsp")){ %> class="active" <%}%>><a href="/admin/other?type=banner">BANNER</a></li>
                <li <% if(pageName.equals("partner.jsp")){ %> class="active" <%}%>><a href="/admin/partners">ĐỐI TÁC</a></li>
                <li <% if(pageName.equals("contact.jsp")){ %> class="active" <%}%>><a href="/admin/other?type=contact">LIÊN HỆ</a></li>
                <li <% if(pageName.equals("recruit.jsp")){ %> class="active" <%}%>><a href="/admin/other?type=recruit">TUYỂN DỤNG</a></li>
                <li <% if(pageName.equals("footer.jsp")){ %> class="active" <%}%>><a href="/admin/other?type=footer">FOOTER</a></li>
                <li <% if(pageName.equals("image.jsp")){ %> class="active" <%}%>><a href="/admin/image">HÌNH ẢNH</a></li>
                <li <% if(pageName.equals("feedback.jsp")){ %> class="active" <%}%>><a href="/admin/image">FEEDBACK</a></li>
                <li <% if(pageName.equals("carouselImage.jsp")){ %> class="active" <%}%>><a href="/admin/slideImage">SLIDE SHOW</a></li>
                <li><a href="/admin/signout">ĐĂNG XUẤT</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid" style="height: 100vh;">
    <div class="row content" style="height: 100vh;">
        <div class="col-sm-3 sidenav hidden-xs">
            <h2>PHÂN PHỐI VIỆT</h2>
            <ul class="nav nav-pills nav-stacked">
                <li <% if(pageName.equals("product.jsp")){ %> class="active" <%}%>><a href="/admin/product">SẢN PHẨM</a></li>
                <li <% if(pageName.equals("brand.jsp")){ %> class="active" <%}%>><a href="/admin/brand">THƯƠNG HIỆU</a></li>
                <li <% if(pageName.equals("news.jsp")){ %> class="active" <%}%>><a href="/admin/news">TIN TỨC</a></li>
                <li <% if(pageName.equals("introduction.jsp")){ %> class="active" <%}%>><a href="/admin/introduction">GIỚI THIỆU</a></li>
                <li <% if(pageName.equals("users.jsp")){ %> class="active" <%}%>><a href="/admin/users">NGƯỜI DÙNG</a></li>
                <li <% if(pageName.equals("banner.jsp")){ %> class="active" <%}%>><a href="/admin/other?type=banner">BANNER</a></li>
                <li <% if(pageName.equals("partner.jsp")){ %> class="active" <%}%>><a href="/admin/partners">ĐỐI TÁC</a></li>
                <li <% if(pageName.equals("contact.jsp")){ %> class="active" <%}%>><a href="/admin/other?type=contact">LIÊN HỆ</a></li>
                <li <% if(pageName.equals("recruit.jsp")){ %> class="active" <%}%>><a href="/admin/other?type=recruit">TUYỂN DỤNG</a></li>
                <li <% if(pageName.equals("footer.jsp")){ %> class="active" <%}%>><a href="/admin/other?type=footer">FOOTER</a></li>
                <li <% if(pageName.equals("image.jsp")){ %> class="active" <%}%>><a href="/admin/image">HÌNH ẢNH</a></li>
                <li <% if(pageName.equals("feedback.jsp")){ %> class="active" <%}%>><a href="/admin/image">FEEDBACK</a></li>
                <li <% if(pageName.equals("carouselImage.jsp")){ %> class="active" <%}%>><a href="/admin/slideImage">SLIDE SHOW</a></li>
                <li><a href="/admin/signout">ĐĂNG XUẤT</a></li>
            </ul><br>
        </div>
        <br>

        <div class="col-sm-9 col-lg-9 col-sm-12 col-xs-12">
            <form:form modelAttribute="editOtherForm" method="post">
                <div class="form-group">
                    <form:textarea cssClass="form-control" path="Content" id="Content" value="${editOtherForm.getContent()}"/>
                </div>
                <div class="text-center" style="margin-bottom: 60px;">
                    <button type="submit" class="text-center btn btn-primary">SỬA</button>
                </div>
            </form:form>
            <script type="application/javascript">
                tinyMCE.init({
                    selector: "textarea",
                    height: 500,
                    menubar: false,
                    plugins: [
                        'advlist autolink lists link image charmap print preview anchor',
                        'searchreplace visualblocks code fullscreen',
                        'insertdatetime media table contextmenu paste code'
                    ],
                    toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                    content_css: [
                        '${generalCss}', '${TinyMCE_Css}'],
                    image_class_list: [{title: 'Responsive', value: 'img-responsive'}]
                });
            </script>
        </div>
    </div>
</div>
</body>
</html>
