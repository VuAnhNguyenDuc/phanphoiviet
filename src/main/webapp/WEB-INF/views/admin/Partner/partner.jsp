<%--
  Created by IntelliJ IDEA.
  User: Vu Anh Nguyen Duc
  Date: 6/23/2017
  Time: 02:07
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>ĐỐI TÁC</title>
</head>
<body>
<p><a href="/admin/partners/insert" class="btn btn-primary">THÊM ĐỐI TÁC MỚI</a></p>
<div class="table-responsive" style="width: 100%;">
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Mã</th>
            <th>Tên đối tác</th>
            <th>Đường dẫn</th>
            <th class="hidden-xs">Ảnh đại diện</th>
            <th>Trạng thái</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${partnerList}" var="partner">
            <tr>
                <td>${partner.getID()}</td>
                <td>${partner.getName()}</td>
                <td>${partner.getAddress()}</td>
                <td class="item-img hidden-xs"><img src="${partner.getPath()}" class="img-responsive zoom" alt="brandImg" data-magnify-src="${partner.getPath()}"></td>
                <td>
                    <c:if test="${partner.getStatus() == 1}">
                        Hoạt động
                    </c:if>
                    <c:if test="${partner.getStatus() == -1}">
                        Vô hiệu
                    </c:if>
                    <c:if test="${partner.getStatus() == 2}">
                        Nổi bật
                    </c:if>
                </td>
                <td>
                    <a href="/admin/partners/update?id=${partner.getID()}" class="btn btn-primary">Chỉnh sửa</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>
