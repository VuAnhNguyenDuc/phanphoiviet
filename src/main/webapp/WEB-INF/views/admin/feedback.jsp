<%--
  Created by IntelliJ IDEA.
  User: Vu Anh Nguyen Duc
  Date: 6/23/2017
  Time: 02:28
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>FEEDBACK NGƯỜI DÙNG</title>
</head>
<body>
    <div class="table-responsive" style="width: 100%;">
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Mã</th>
            <th>Tên người dùng</th>
            <th>Email</th>
            <th>Điện thoại</th>
            <th>Nội dung</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${feedbackList}" var="feedback">
            <tr>
                <td>${feedback.getID()}</td>
                <td>${feedback.getName()}</td>
                <td>${feedback.getEmail()}</td>
                <td>${feedback.getPhone()}</td>
                <td>${feedback.getContent()}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>
