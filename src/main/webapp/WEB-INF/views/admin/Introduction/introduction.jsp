<%--
  Created by IntelliJ IDEA.
  User: Vu Anh Nguyen Duc
  Date: 6/26/2017
  Time: 21:34
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>GIỚI THIỆU</title>
</head>
<body>
<p><a href="/admin/introduction/insert" class="btn btn-primary">THÊM GIỚI THIỆU MỚI</a></p>
<div class="table-responsive" style="width: 100%;">
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Mã</th>
            <th>Tiêu đề</th>
            <th>Nội dung</th>
            <th class="hidden-xs">Ảnh đại diện</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${introductionList}" var="introduction">
            <tr>
                <td>${introduction.getID()}</td>
                <td>${introduction.getTitle()}</td>
                <td>
                    <a href="/admin/introduction/content?id=${introduction.getID()}" class="btn btn-primary">Chi tiết...</a>
                </td>
                <td class="item-img hidden-xs"><img src="${introduction.getPath()}" class="img-responsive zoom" alt="brandImg" data-magnify-src="${introduction.getPath()}"></td>
                <td>
                    <a href="/admin/introduction/update?id=${introduction.getID()}" class="btn btn-primary">Chỉnh sửa</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>
