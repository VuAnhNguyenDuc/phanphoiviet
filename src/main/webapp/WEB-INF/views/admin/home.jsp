<%--
  Created by IntelliJ IDEA.
  User: Vu Anh Nguyen Duc
  Date: 6/18/2017
  Time: 13:26
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
    String pageName = request.getAttribute("pageName").toString();
%>
<html>
<head lang="vi">
    <%if(pageName.equals("home.jsp")){%>
        <title>TRANG ADMIN</title>
    <%}%>
    <meta name="keyword" content="phanphoiviet,Phân Phối Việt,Royal Dansk,bánh Royal Dansk, bán bánh Royal Danks, ban banh Royal Danks">
    <meta name="description" content="Công ty TNHH Phân Phối Việt, chuyên cung cấp các sản phẩm bánh Royal Danks tại Việt Nam">
    <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet prefetch" href="http://thdoan.github.io/magnify/css/magnify.css">

    <style type="text/css">
        body{
            font-weight: bold;
        }
        /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
        .row.content {
            height: inherit;
            display: flex;
        }

        /* Set gray background color and 100% height */
        .sidenav {
            background-color: #f1f1f1;
            align-items: stretch;
        }

        .item-img img{
            max-width: 400px !important;
            max-height: 300px !important;
        }

        /* On small screens, set height to 'auto' for the grid */
        @media screen and (max-width: 767px) {
            .row.content {height: auto;}
        }
    </style>
</head>
<body>

<nav class="navbar navbar-inverse visible-xs">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/admin">PHÂN PHỐI VIỆT</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li <% if(pageName.equals("product.jsp")){ %> class="active" <%}%>><a href="/admin/product">SẢN PHẨM</a></li>
                <li <% if(pageName.equals("brand.jsp")){ %> class="active" <%}%>><a href="/admin/brand">THƯƠNG HIỆU</a></li>
                <li <% if(pageName.equals("news.jsp")){ %> class="active" <%}%>><a href="/admin/news">TIN TỨC</a></li>
                <li <% if(pageName.equals("introduction.jsp")){ %> class="active" <%}%>><a href="/admin/introduction">GIỚI THIỆU</a></li>
                <li <% if(pageName.equals("users.jsp")){ %> class="active" <%}%>><a href="/admin/users">NGƯỜI DÙNG</a></li>
                <li <% if(pageName.equals("image.jsp")){ %> class="active" <%}%>><a href="/admin/image">HÌNH ẢNH</a></li>
                <li <% if(pageName.equals("banner.jsp")){ %> class="active" <%}%>><a href="/admin/other?type=banner">BANNER</a></li>
                <li <% if(pageName.equals("partner.jsp")){ %> class="active" <%}%>><a href="/admin/partners">ĐỐI TÁC</a></li>
                <li <% if(pageName.equals("contact.jsp")){ %> class="active" <%}%>><a href="/admin/other?type=contact">LIÊN HỆ</a></li>
                <li <% if(pageName.equals("recruit.jsp")){ %> class="active" <%}%>><a href="/admin/other?type=recruit">TUYỂN DỤNG</a></li>
                <li <% if(pageName.equals("footer.jsp")){ %> class="active" <%}%>><a href="/admin/other?type=footer">FOOTER</a></li>
                <li <% if(pageName.equals("image.jsp")){ %> class="active" <%}%>><a href="/admin/image">HÌNH ẢNH</a></li>
                <li <% if(pageName.equals("feedback.jsp")){ %> class="active" <%}%>><a href="/admin/feedback">FEEDBACK</a></li>
                <li <% if(pageName.equals("carouselImage.jsp")){ %> class="active" <%}%>><a href="/admin/slideImage">SLIDE SHOW</a></li>
                <li><a href="/admin/signout">ĐĂNG XUẤT</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row content" style="min-height: 100vh;">
        <div class="col-sm-3 sidenav hidden-xs" style="min-height: 100vh;">
            <h2>PHÂN PHỐI VIỆT</h2>
            <ul class="nav nav-pills nav-stacked">
                <li <% if(pageName.equals("product.jsp")){ %> class="active" <%}%>><a href="/admin/product">SẢN PHẨM</a></li>
                <li <% if(pageName.equals("brand.jsp")){ %> class="active" <%}%>><a href="/admin/brand">THƯƠNG HIỆU</a></li>
                <li <% if(pageName.equals("news.jsp")){ %> class="active" <%}%>><a href="/admin/news">TIN TỨC</a></li>
                <li <% if(pageName.equals("introduction.jsp")){ %> class="active" <%}%>><a href="/admin/introduction">GIỚI THIỆU</a></li>
                <li <% if(pageName.equals("users.jsp")){ %> class="active" <%}%>><a href="/admin/users">NGƯỜI DÙNG</a></li>
                <li <% if(pageName.equals("banner.jsp")){ %> class="active" <%}%>><a href="/admin/other?type=banner">BANNER</a></li>
                <li <% if(pageName.equals("partner.jsp")){ %> class="active" <%}%>><a href="/admin/partners">ĐỐI TÁC</a></li>
                <li <% if(pageName.equals("contact.jsp")){ %> class="active" <%}%>><a href="/admin/other?type=contact">LIÊN HỆ</a></li>
                <li <% if(pageName.equals("recruit.jsp")){ %> class="active" <%}%>><a href="/admin/other?type=recruit">TUYỂN DỤNG</a></li>
                <li <% if(pageName.equals("footer.jsp")){ %> class="active" <%}%>><a href="/admin/other?type=footer">FOOTER</a></li>
                <li <% if(pageName.equals("image.jsp")){ %> class="active" <%}%>><a href="/admin/image">HÌNH ẢNH</a></li>
                <li <% if(pageName.equals("feedback.jsp")){ %> class="active" <%}%>><a href="/admin/feedback">FEEDBACK</a></li>
                <li <% if(pageName.equals("carouselImage.jsp")){ %> class="active" <%}%>><a href="/admin/slideImage">SLIDE SHOW</a></li>
                <li><a href="/admin/signout">ĐĂNG XUẤT</a></li>
            </ul><br>
        </div>
        <br>

        <div class="col-sm-9 col-lg-9 col-sm-12 col-xs-12" style="padding-top: 30px">
            <%if(pageName.equals("product.jsp")){%>
                <%@ include file="products.jsp" %>
            <%}%>
            <%if(pageName.equals("brand.jsp")){%>
                <%@ include file="brands.jsp" %>
            <%}%>
            <%if(pageName.equals("news.jsp")){%>
                <%@ include file="news.jsp" %>
            <%}%>
            <%if(pageName.equals("users.jsp")){%>
                <%@ include file="users.jsp" %>
            <%}%>
            <%if(pageName.equals("image.jsp")){%>
                <%@ include file="image.jsp" %>
            <%}%>
            <%if(pageName.equals("partners.jsp")){%>
                <%@ include file="Partner/partner.jsp" %>
            <%}%>
            <%if(pageName.equals("feedback.jsp")){%>
                <%@ include file="feedback.jsp" %>
            <%}%>
            <%if(pageName.equals("slideImage.jsp")){%>
                <%@ include file="SlideImage/slideImage.jsp" %>
            <%}%>
            <%if(pageName.equals("get-news-content.jsp")){%>
                ${Content}
            <%}%>
            <%if(pageName.equals("get-introduction-content.jsp")){%>
                ${Content}
            <%}%>
            <%if(pageName.equals("introduction.jsp")){%>
                 <%@ include file="Introduction/introduction.jsp" %>
            <%}%>
        </div>
    </div>
</div>

<script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="application/javascript" src="http://thdoan.github.io/magnify/js/jquery.magnify.js"></script>
<script type="application/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script type="application/javascript">
    $(document).ready(function() {
        // Initiate zoom
        //$('.zoom').magnify();
    })
</script>

</body>
</html>
