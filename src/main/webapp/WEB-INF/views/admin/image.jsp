<%--
  Created by IntelliJ IDEA.
  User: Vu Anh Nguyen Duc
  Date: 6/18/2017
  Time: 16:16
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>HÌNH ẢNH</title>
</head>
<body>
<p><a href="/admin/image/insert" class="btn btn-primary">THÊM HÌNH ẢNH MỚI</a></p>
<div class="table-responsive" style="width: 100%;">
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Mã</th>
            <th>Đường dẫn</th>
            <th class="hidden-xs">Hình ảnh</th>
        </tr>
        </thead>
        <tbody>
            <c:forEach items="${imageList}" var="image">
                <tr>
                    <td>${image.getID()}</td>
                    <td>${image.getPath()}</td>
                    <td class="item-img hidden-xs"><img src="${image.getPath()}"  class="img-responsive zoom" data-magnify-src="${image.getPath()}" alt="brandImg"></td>
                    <td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>

</body>
</html>
