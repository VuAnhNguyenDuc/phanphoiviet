package com.form;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Vu Anh Nguyen Duc on 6/18/2017.
 */
public class InsertEditImageForm {
    private MultipartFile Image;

    public MultipartFile getImage() {
        return Image;
    }

    public void setImage(MultipartFile image) {
        Image = image;
    }
}
