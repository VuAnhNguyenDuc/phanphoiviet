package com.form;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Size;

/**
 * Created by Vu Anh Nguyen Duc on 6/18/2017.
 */
public class InsertEditBrandForm {

    @Size(min = 1, max = 255, message = "Tên thương hiệu phải từ 1 đến 255 ký tự")
    private String Name;

    @Size(min = 1, max = 255, message = "Xuất xứ phải từ 1 đến 255 ký tự")
    private String Origin;

    private MultipartFile Path;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getOrigin() {
        return Origin;
    }

    public void setOrigin(String origin) {
        Origin = origin;
    }

    public MultipartFile getPath() {
        return Path;
    }

    public void setPath(MultipartFile path) {
        Path = path;
    }
}
