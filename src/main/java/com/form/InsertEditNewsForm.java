package com.form;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Vu Anh Nguyen Duc on 6/18/2017.
 */
public class InsertEditNewsForm {
    @Size(min = 10, max = 255, message = "Tiêu đề phải từ 10 đến 255 ký tự")
    private String Title;

    private String AddedDate;

    @Size(min = 10, message = "Nội dung tin tức phải lớn hơn 10 ký tự")
    private String Content;

    private MultipartFile Path;

    @NotNull
    private int Status;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getAddedDate() {
        return AddedDate;
    }

    public void setAddedDate(String addedDate) {
        AddedDate = addedDate;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public MultipartFile getPath() {
        return Path;
    }

    public void setPath(MultipartFile path) {
        Path = path;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }
}
