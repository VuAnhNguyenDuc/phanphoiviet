package com.form;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Vu Anh Nguyen Duc on 6/23/2017.
 */
public class InsertEditSlideImageForm {
    private MultipartFile Path;
    private int Status;

    public MultipartFile getPath() {
        return Path;
    }

    public void setPath(MultipartFile path) {
        Path = path;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }
}
