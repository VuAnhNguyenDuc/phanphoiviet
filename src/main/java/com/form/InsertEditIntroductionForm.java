package com.form;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Vu Anh Nguyen Duc on 6/26/2017.
 */
public class InsertEditIntroductionForm {
    private String Title;
    private String Content;
    private MultipartFile Path;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public MultipartFile getPath() {
        return Path;
    }

    public void setPath(MultipartFile path) {
        Path = path;
    }
}
