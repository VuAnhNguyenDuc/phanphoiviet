package com.form;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Vu Anh Nguyen Duc on 6/18/2017.
 */
public class InsertEditProductForm {
    @Size(min = 1, max = 255, message = "Tên sản phẩm phải từ 1 đến 255 ký tự")
    private String Name;

    private String Weight;

    @NotNull(message = "Bạn chưa thêm thương hiệu của sản phẩm")
    private int BrandID;

    private MultipartFile Path;

    @NotNull(message = "Chưa thêm trạng thái cho sản phẩm")
    private int Status;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }

    public int getBrandID() {
        return BrandID;
    }

    public void setBrandID(int brandID) {
        BrandID = brandID;
    }

    public MultipartFile getPath() {
        return Path;
    }

    public void setPath(MultipartFile path) {
        Path = path;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }
}
