package com.form;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Vu Anh Nguyen Duc on 6/23/2017.
 */
public class InsertEditPartnerForm {
    private int ID;
    @NotEmpty(message = "Đây là trường bắt buộc")
    private String Name;
    private String Address;
    private MultipartFile ImageFile;
    private int Status;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public MultipartFile getImageFile() {
        return ImageFile;
    }

    public void setImageFile(MultipartFile imageFile) {
        ImageFile = imageFile;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }
}
