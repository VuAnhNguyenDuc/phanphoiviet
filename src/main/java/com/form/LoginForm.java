package com.form;

import javax.validation.constraints.Size;

/**
 * Created by Vu Anh Nguyen Duc on 6/18/2017.
 */
public class LoginForm {
    @Size(min = 8, max = 45, message = "Tên đăng nhập phải từ 8 đến 45 ký tự")
    private String Username;

    @Size(min = 8, max = 45, message = "Mật khẩu phải từ 8 đến 45 ký tự")
    private String Password;

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
