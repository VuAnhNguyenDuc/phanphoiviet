package com.form;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * Created by Vu Anh Nguyen Duc on 6/23/2017.
 */
public class InsertFeedbackForm {
    private int ID;
    @NotEmpty(message = "Đây là trường bắt buộc")
    private String Name;
    private String Email;
    private String Phone;
    @Size(min = 10,message = "Vui lòng nhập vào hơn 10 ký tự")
    private String Content;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }
}
