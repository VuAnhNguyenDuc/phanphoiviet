package com.form;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Vu Anh Nguyen Duc on 6/18/2017.
 */
public class InsertEditUserForm {
    @Size(min = 8,max = 45, message = "Tên đăng nhập phải từ 1 đến 255 ký tự")
    private String Username;

    @Size(min = 8, max = 45, message = "Mật khẩu phải từ 1 đến 255 ký tự")
    private String Password;

    @Size(min = 1, max = 10, message = "Kiểu người dùng phải từ 1 đến 10 ký tự")
    private String Type;

    @NotNull(message = "Bạn chưa thêm trạng thái của người dùng")
    private int Status;

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }
}
