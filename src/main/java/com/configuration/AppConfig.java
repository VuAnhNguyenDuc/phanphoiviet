package com.configuration;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/**
 * Created by Vu Anh Nguyen Duc on 11/30/2016.
 */

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com")
public class AppConfig extends WebMvcConfigurerAdapter {
    @Bean
    public ViewResolver viewResolver(){
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }
    // for uploading files
    @Bean
    public CommonsMultipartResolver multipartResolver(){
        CommonsMultipartResolver resolver = new CommonsMultipartResolver();
        resolver.setDefaultEncoding("utf-8");
        return resolver;
    }
    // include css and js files
    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry
                                            registry){
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/web/");

        registry.addResourceHandler("/admin/**").addResourceLocations("/resources/admin/");
        registry.addResourceHandler("/admin/brand/**").addResourceLocations("/resources/admin/");
        registry.addResourceHandler("/admin/news/**").addResourceLocations("/resources/admin/");
        registry.addResourceHandler("/admin/product/**").addResourceLocations("/resources/admin/");
        registry.addResourceHandler("/admin/users/**").addResourceLocations("/resources/admin/");
        registry.addResourceHandler("/admin/image/**").addResourceLocations("/resources/admin/");
        registry.addResourceHandler("/admin/partners/**").addResourceLocations("/resources/admin/");
        registry.addResourceHandler("/admin/slideImage/**").addResourceLocations("/resources/admin/");
        registry.addResourceHandler("/admin/introduction/**").addResourceLocations("/resources/admin/");

        registry.addResourceHandler("/images/**").addResourceLocations("/resources/images/");

    }
    @Bean
    public MessageSource messageSource(){
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("messages");
        return messageSource;
    }
}
