package com.hibernate;

import com.model.Introduction;
import com.model.IntroductionView;
import com.util.HibernateUtil;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by Vu Anh Nguyen Duc on 6/26/2017.
 */
public class IntroductionHibernate extends GeneralHibernate<Integer,Introduction,IntroductionView> {
    Session session;

    // GET A LIST OF INTRODUCTION (IN VIEW)
    public List<IntroductionView> getAllIntroductions(){
        return getEntities("from IntroductionView i order by i.ID");
    }

    // INSERT A NEW INTRODUCTION AND RETURN IT'S ID
    public int saveIntroduction(Introduction introduction){
        // Chua set introduction ImageID do!!!
        return saveAndGetID(introduction);
    }

    // RETURN ONE INTRODUCTION BY IT'S ID
    public Introduction getIntroduction(Integer ID){
        return getEntity(ID);
    }


    // UPDATE ONE INTRODUCTION BY IT'S ID
    @Override
    public void updateEntity(Introduction introduction){
        Introduction entity = getIntroduction(introduction.getID());
        session = HibernateUtil.openSession();
        session.beginTransaction();

        entity.setContent(introduction.getContent());
        entity.setTitle(introduction.getTitle());
        entity.setImageID(introduction.getImageID());

        session.saveOrUpdate(entity);
        HibernateUtil.SessionCommit(session);
    }

    public void updateIntroduction(Introduction introduction){
        updateEntity(introduction);
    }
}
