package com.hibernate;

import com.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.metadata.ClassMetadata;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by Vu Anh Nguyen Duc on 6/19/2017.
 */

/*
    PK is the type of the primary key
    T is the name of the class
 */
@SuppressWarnings("unchecked")
public class GeneralHibernate<PK extends Serializable, T, K> {
    Session session;
    private final Class<T> persistentClass;

    protected GeneralHibernate(){
        this.persistentClass = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }

    protected List<K> getEntities(String query){
        session = HibernateUtil.openSession();
        session.beginTransaction();

        List<K> entities = (List<K>) session.createQuery(query).list();
        HibernateUtil.SessionCommit(session);
        return entities;
    }

    protected PK saveAndGetID(T entity){
        session = HibernateUtil.openSession();
        session.beginTransaction();

        PK id = (PK) session.save(entity);
        HibernateUtil.SessionCommit(session);
        return id;
    }

    protected T getEntity(PK key){
        session = HibernateUtil.openSession();
        session.beginTransaction();

        T entity = (T) session.get(persistentClass,key);

        HibernateUtil.SessionCommit(session);
        return entity;
    }

    protected void updateEntity(T newEntity){

    }

}
