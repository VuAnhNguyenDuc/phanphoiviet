package com.hibernate;

import com.model.Brand;
import com.model.Product;
import com.model.ProductView;
import com.util.HibernateUtil;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by Vu Anh Nguyen Duc on 6/19/2017.
 */
@SuppressWarnings("unchecked")
public class ProductHibernate extends GeneralHibernate<Integer,Product,ProductView> {
    Session session;
    private static BrandHibernate brandHibernate = new BrandHibernate();

    // GET A LIST OF PRODUCTS (IN VIEW)
    public List<ProductView> getAllProduct(){
        return getEntities("from ProductView p ORDER BY p.ID desc");
    }

    public List<ProductView> showEnabledProducts(){
        return getEntities("from ProductView p where p.Status>0 ORDER BY p.ID desc");
    }

    // GET HOT PRODUCT
    public List<ProductView> getHotProduct(){
        return getEntities("from ProductView p where p.Status=2 ORDER BY p.ID desc");
    }

    // GET PRODUCTS BY IT'S BRAND ID
    public List<ProductView> getProductsByBrand(int id){
        String brandName = brandHibernate.getBrand(id).getName();
        return getEntities("from ProductView p where p.BrandName='"+ brandName +"' ORDER BY p.ID desc");
    }


    // INSERT A NEW PRODUCT AND RETURN IT'S ID
    public int saveProduct(Product product){
        // Chua set news ImageID do!!!
        return saveAndGetID(product);
    }

    // RETURN ONE PRODUCT BY IT'S ID
    public Product getProduct(Integer ID){
        return getEntity(ID);
    }

    // RETURN ONE PRODUCT VIEW BY IT'S ID
    public ProductView getProductView(Integer ID){
        return getEntities("from ProductView p where p.ID="+ID).get(0);}

    // UPDATE ONE PRODUCT BY IT'S ID
    @Override
    public void updateEntity(Product product){
        Product entity = getProduct(product.getID());
        session = HibernateUtil.openSession();
        session.beginTransaction();

        entity.setName(product.getName());
        entity.setWeight(product.getWeight());
        entity.setImageID(product.getImageID());
        entity.setBrandID(product.getBrandID());
        entity.setStatus(product.getStatus());

        session.saveOrUpdate(entity);
        HibernateUtil.SessionCommit(session);
    }

    public void updateProduct(Product newProduct){
        updateEntity(newProduct);
    }
}
