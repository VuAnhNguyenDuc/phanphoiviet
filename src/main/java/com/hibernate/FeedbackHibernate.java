package com.hibernate;

import com.model.Feedback;
import com.util.HibernateUtil;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by Vu Anh Nguyen Duc on 6/23/2017.
 */
public class FeedbackHibernate extends GeneralHibernate<Integer, Feedback,Feedback> {
    Session session;

    // GET A LIST OF FEEDBACK (IN VIEW)
    public List<Feedback> getAllFeedback(){
        return getEntities("from Feedback f ORDER BY f.ID asc");
    }

    // INSERT A NEW FEEDBACK AND RETURN IT'S ID
    public int saveFeedback(Feedback feedback){
        return saveAndGetID(feedback);
    }

    // RETURN ONE FEEDBACK BY IT'S ID
    public Feedback getFeedback(Integer ID){
        return getEntity(ID);
    }

    // UPDATE ONE FEEDBACK BY IT'S ID
    @Override
    public void updateEntity(Feedback feedback){
        Feedback entity = getFeedback(feedback.getID());
        session = HibernateUtil.openSession();
        session.beginTransaction();

        entity.setName(feedback.getName());
        entity.setContent(feedback.getContent());
        entity.setEmail(feedback.getEmail());
        entity.setPhone(feedback.getPhone());

        session.saveOrUpdate(entity);
        HibernateUtil.SessionCommit(session);
    }

    public void updateFeedback(Feedback newFeedback){
        updateEntity(newFeedback);
    }
}
