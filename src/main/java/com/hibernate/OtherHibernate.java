package com.hibernate;

import com.model.Other;
import com.util.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * Created by Vu Anh Nguyen Duc on 6/21/2017.
 */
public class OtherHibernate extends GeneralHibernate<String,Other,Other> {
    Session session;

    // RETURN ONE OTHER BY IT'S TYPE
    public Other getOther(String Type){
        return getEntity(Type);
    }

    // UPDATE ONE OTHER INSTANCE
    @Override
    public void updateEntity(Other other){
        Other entity = getOther(other.getType());
        session = HibernateUtil.openSession();
        session.beginTransaction();

        entity.setContent(other.getContent());
        session.saveOrUpdate(entity);
        HibernateUtil.SessionCommit(session);
    }

    public void updateOther(Other other){
        updateEntity(other);
    }
}
