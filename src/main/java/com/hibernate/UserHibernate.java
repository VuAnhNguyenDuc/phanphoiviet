package com.hibernate;

import com.model.User;
import com.util.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by Vu Anh Nguyen Duc on 6/19/2017.
 */
@SuppressWarnings("unchecked")
public class UserHibernate extends GeneralHibernate<Integer,User,User> {
    Session session;

    // GET A LIST OF USER
    public List<User> getAllUser(){
        session = HibernateUtil.openSession();
        session.beginTransaction();

        List<User> users = (List<User>) session.createQuery("from User u ORDER BY u.ID asc ").list();
        HibernateUtil.SessionCommit(session);
        return users;
    }

    // INSERT A NEW USER AND RETURN IT'S ID
    public int saveUser(User user){
        return saveAndGetID(user);
    }

    // RETURN ONE USER BY IT'S ID
    public User getUser(Integer ID){
        return getEntity(ID);
    }

    // RETURN ONE ADMIN BY IT'S USERNAME AND PASSWORD
    public User getAdmin(String Username,String Password){
        session = HibernateUtil.openSession();
        session.beginTransaction();

        String hql = "from User u where u.Password=? and  u.Username=? and u.Type = 'admin' and u.Status = 1";
        Query result = session.createQuery(hql).setParameter(0,Password).setParameter(1,Username);
        List list = result.list();
        return (list.size() > 0) ? (User) list.get(0) : null;
    }

    // UPDATE ONE USER BY IT'S ID
    @Override
    public void updateEntity(User user){
        User entity = getUser(user.getID());
        session = HibernateUtil.openSession();
        session.beginTransaction();

        entity.setPassword(user.getPassword());
        entity.setStatus(user.getStatus());
        entity.setType(user.getType());
        entity.setUsername(user.getUsername());

        session.saveOrUpdate(entity);
        HibernateUtil.SessionCommit(session);
    }

    public void updateUser(User newUser){
        updateEntity(newUser);
    }
}
