package com.hibernate;

import com.model.News;
import com.model.NewsView;
import com.util.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by Vu Anh Nguyen Duc on 6/19/2017.
 */
@SuppressWarnings("unchecked")
public class NewsHibernate extends GeneralHibernate<Integer, News,NewsView> {
    Session session;
    public String getDateFormat(int newsID){
        session = HibernateUtil.openSession();
        session.beginTransaction();

        Query query = session.createQuery("select DATE_FORMAT(n.AddedDate, '%d-%m-%Y') from News as n where n.ID=?");
        query.setParameter(0,newsID);
        String result = (String) query.list().get(0);
        HibernateUtil.SessionCommit(session);
        return result;
    }

    // GET A LIST OF NEWS (IN VIEW)
    public List<NewsView> getAllNews(){
        return getEntities("from NewsView n ORDER BY n.AddedDate desc");
    }

    public List<NewsView> showEnabledNews(){return getEntities("from NewsView n where n.Status>0 ORDER BY n.ID desc");}

    // GET HOT VIEW
    public List<NewsView> getHotNews(){
        return getEntities("from NewsView n where n.Status=2 ORDER BY n.ID desc");
    }

    // INSERT A NEW NEWS AND RETURN IT'S ID
    public int saveNews(News news){
        // Chua set news ImageID do!!!
        return saveAndGetID(news);
    }

    // RETURN ONE NEWS BY IT'S ID
    public News getNews(Integer ID){
        return getEntity(ID);
    }

    // RETURN ONE NEWS VIEW BY IT'S ID
    public NewsView getNewsView(Integer ID){
        return getEntities("from NewsView n where n.ID="+ID).get(0);}

    // UPDATE ONE NEWS BY IT'S ID
    @Override
    public void updateEntity(News news){
        News entity = getNews(news.getID());
        session = HibernateUtil.openSession();
        session.beginTransaction();

        entity.setTitle(news.getTitle());
        entity.setContent(news.getContent());
        entity.setImageID(news.getImageID());
        entity.setStatus(news.getStatus());

        session.saveOrUpdate(entity);
        HibernateUtil.SessionCommit(session);
    }

    public void updateNews(News newNews){
        updateEntity(newNews);
    }
}
