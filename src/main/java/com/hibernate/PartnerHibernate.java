package com.hibernate;

import com.model.Partner;
import com.model.PartnerView;
import com.util.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by Vu Anh Nguyen Duc on 6/23/2017.
 */
public class PartnerHibernate extends GeneralHibernate<Integer,Partner,PartnerView> {
    Session session;
    
    // GET A LIST OF PARTNER (IN VIEW)
    public List<PartnerView> getAllPartner(){
        return getEntities("from PartnerView p ORDER BY p.ID asc");
    }

    public List<PartnerView> showEnabledPartner(){return getEntities("from PartnerView p where p.Status>0 order by p.Status desc");}

    // GET HOT VIEW
    public List<PartnerView> getHotPartner(){
        return getEntities("from PartnerView p where p.Status=2 ORDER BY p.ID asc");
    }

    // INSERT A NEW PARTNER AND RETURN IT'S ID
    public int savePartner(Partner partner){
        // Chua set partner ImageID do!!!
        return saveAndGetID(partner);
    }

    // RETURN ONE PARTNER BY IT'S ID
    public Partner getPartner(Integer ID){
        return getEntity(ID);
    }

    // RETURN ONE PARTNER VIEW BY IT'S ID
    public PartnerView getPartnerView(Integer ID){
        return getEntities("from PartnerView p where p.ID="+ID).get(0);}

    // UPDATE ONE PARTNER BY IT'S ID
    @Override
    public void updateEntity(Partner partner){
        Partner entity = getPartner(partner.getID());
        session = HibernateUtil.openSession();
        session.beginTransaction();

        entity.setStatus(partner.getStatus());
        entity.setAddress(partner.getAddress());
        entity.setImageID(partner.getImageID());
        entity.setName(partner.getName());

        session.saveOrUpdate(entity);
        HibernateUtil.SessionCommit(session);
    }

    public void updatePartner(Partner newPartner){
        updateEntity(newPartner);
    }
}
