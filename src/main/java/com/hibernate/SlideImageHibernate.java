package com.hibernate;

import com.model.SlideImage;
import com.model.SlideImageView;
import com.util.HibernateUtil;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by Vu Anh Nguyen Duc on 6/23/2017.
 */
public class SlideImageHibernate extends GeneralHibernate<Integer,SlideImage,SlideImageView
        > {
    Session session;

    // GET A LIST OF SLIDE IMAGE (IN VIEW)
    public List<SlideImageView> getAllSlideImages(){
        return getEntities("from SlideImageView s ORDER BY s.ID");
    }

    public List<SlideImageView> showEnabledSlideImages(){
        return getEntities("from SlideImageView s where s.Status=1 ORDER BY s.ID");
    }

    // INSERT A NEW SLIDE IMAGE AND RETURN IT'S ID
    public int saveSlideImage(SlideImage slideImage){
        // Chua set slideImage ImageID do!!!
        return saveAndGetID(slideImage);
    }

    // RETURN ONE SLIDE IMAGE BY IT'S ID
    public SlideImage getSlideImage(Integer ID){
        return getEntity(ID);
    }

    // RETURN ONE SLIDE IMAGE BY IT'S NAME
    public SlideImageView getSlideImageView(Integer ID){
        return getEntities("from SlideImageView s where s.ID="+ID).get(0);
    }

    // UPDATE ONE SLIDE IMAGE BY IT'S ID
    @Override
    public void updateEntity(SlideImage slideImage){
        SlideImage entity = getSlideImage(slideImage.getID());
        session = HibernateUtil.openSession();
        session.beginTransaction();

        entity.setImageID(slideImage.getImageID());

        session.saveOrUpdate(entity);
        HibernateUtil.SessionCommit(session);
    }

    public void updateSlideImage(SlideImage slideImage){
        updateEntity(slideImage);
    }
}
