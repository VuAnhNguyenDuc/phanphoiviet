package com.hibernate;

import com.model.Brand;
import com.model.Image;
import com.util.HibernateUtil;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by Vu Anh Nguyen Duc on 6/19/2017.
 */
@SuppressWarnings("unchecked")
public class ImageHibernate extends GeneralHibernate<Integer, Image,Image>  {
    Session session;

    // GET A LIST OF IMAGE
    public List<Image> getAllImages(){
        session = HibernateUtil.openSession();
        session.beginTransaction();

        List<Image> images = (List<Image>) session.createQuery("from Image i ORDER BY i.ID DESC ").list();
        HibernateUtil.SessionCommit(session);
        return images;
    }

    // INSERT A NEW IMAGE AND RETURN IT'S ID
    public int saveImage(Image image){
        return saveAndGetID(image);
    }

    // RETURN ONE IMAGE BY IT'S ID
    public Image getImage(Integer ID){
        return getEntity(ID);
    }

    // UPDATE ONE IMAGE BY IT'S ID
    @Override
    public void updateEntity(Image image){
        Image entity = getImage(image.getID());
        session = HibernateUtil.openSession();
        session.beginTransaction();

        entity.setPath(image.getPath());

        session.saveOrUpdate(entity);
        HibernateUtil.SessionCommit(session);
    }

    public void updateImage(Image newImage){
        updateEntity(newImage);
    }

}
