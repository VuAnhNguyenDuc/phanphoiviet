package com.hibernate;

import com.model.Brand;
import com.model.BrandView;
import com.util.HibernateUtil;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by Vu Anh Nguyen Duc on 6/19/2017.
 */
@SuppressWarnings("unchecked")
public class BrandHibernate extends GeneralHibernate<Integer,Brand,BrandView> {
    Session session;

    // GET A LIST OF BRAND (IN VIEW)
    public List<BrandView> getAllBrands(){
        return getEntities("from BrandView b ORDER BY b.Name");
    }

    // INSERT A NEW BRAND AND RETURN IT'S ID
    public int saveBrand(Brand brand){
        // Chua set brand ImageID do!!!
        return saveAndGetID(brand);
    }

    // RETURN ONE BRAND BY IT'S ID
    public Brand getBrand(Integer ID){
       return getEntity(ID);
    }

    // RETURN ONE BRAND BY IT'S NAME
    public BrandView getBrandByName(String name){
        return getEntities("from BrandView b where b.Name='"+name+"'").get(0);
    }

    // UPDATE ONE BRAND BY IT'S ID
    @Override
    public void updateEntity(Brand brand){
        Brand entity = getBrand(brand.getID());
        session = HibernateUtil.openSession();
        session.beginTransaction();

        entity.setName(brand.getName());
        entity.setOrigin(brand.getOrigin());
        entity.setImageID(brand.getImageID());

        session.saveOrUpdate(entity);
        HibernateUtil.SessionCommit(session);
    }

    public void updateBrand(Brand brand){
        updateEntity(brand);
    }
}
