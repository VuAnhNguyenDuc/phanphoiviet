package com.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by Vu Anh Nguyen Duc on 6/19/2017.
 */
public class HibernateUtil {
    private static final SessionFactory sessionFactory;

    static{
        try {
            sessionFactory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Session Factory could not be created." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory(){
        return sessionFactory;
    }

    public static Session openSession(){
        return getSessionFactory().openSession();
    }

    public static void SessionCommit(Session session){
        session.getTransaction().commit();
        session.close();
    }
}
