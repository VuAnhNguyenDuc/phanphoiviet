package com.model;

/**
 * Created by Vu Anh Nguyen Duc on 6/19/2017.
 */
public class Brand {
    private Integer ID;
    private String Name;
    private String Origin;
    private Integer ImageID;

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getOrigin() {
        return Origin;
    }

    public void setOrigin(String origin) {
        Origin = origin;
    }

    public Integer getImageID() {
        return ImageID;
    }

    public void setImageID(Integer imageID) {
        ImageID = imageID;
    }
}
