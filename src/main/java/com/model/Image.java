package com.model;

/**
 * Created by Vu Anh Nguyen Duc on 6/18/2017.
 */
public class Image {
    private Integer ID;
    private String Path;

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getPath() {
        return Path;
    }

    public void setPath(String path) {
        Path = path;
    }
}
