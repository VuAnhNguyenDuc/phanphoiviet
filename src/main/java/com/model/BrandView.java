package com.model;

import java.io.Serializable;

/**
 * Created by Vu Anh Nguyen Duc on 6/18/2017.
 */

public class BrandView{
    private Integer ID;
    private String Name;
    private String Origin;
    private String Path;

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getOrigin() {
        return Origin;
    }

    public void setOrigin(String origin) {
        Origin = origin;
    }

    public String getPath() {
        return Path;
    }

    public void setPath(String path) {
        Path = path;
    }
}
