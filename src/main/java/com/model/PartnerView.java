package com.model;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Vu Anh Nguyen Duc on 6/23/2017.
 */
public class PartnerView {
    private int ID;
    private String Name;
    private String Address;
    private String Path;
    private int Status;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getPath() {
        return Path;
    }

    public void setPath(String path) {
        Path = path;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }
}
