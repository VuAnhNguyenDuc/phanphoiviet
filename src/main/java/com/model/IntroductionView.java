package com.model;

/**
 * Created by Vu Anh Nguyen Duc on 6/26/2017.
 */
public class IntroductionView {
    private Integer ID;
    private String Title;
    private String Content;
    private String Path;

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getPath() {
        return Path;
    }

    public void setPath(String path) {
        Path = path;
    }
}
