package com.model;

/**
 * Created by Vu Anh Nguyen Duc on 6/21/2017.
 */
public class Other {

    private String Type;

    private String Content;

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }
}
