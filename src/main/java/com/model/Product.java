package com.model;

/**
 * Created by Vu Anh Nguyen Duc on 6/19/2017.
 */
public class Product {
    private Integer ID;
    private String Name;
    private String Weight;
    private Integer BrandID;
    private Integer ImageID;
    private Integer Status;

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }


    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }


    public Integer getImageID() {
        return ImageID;
    }

    public void setImageID(Integer imageID) {
        ImageID = imageID;
    }

    public Integer getStatus() {
        return Status;
    }

    public void setStatus(Integer status) {
        Status = status;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Integer getBrandID() {
        return BrandID;
    }

    public void setBrandID(Integer brandID) {
        BrandID = brandID;
    }
}
