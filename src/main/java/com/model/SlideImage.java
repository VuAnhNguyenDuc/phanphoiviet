package com.model;

/**
 * Created by Vu Anh Nguyen Duc on 6/23/2017.
 */
public class SlideImage {
    private int ID;
    private int ImageID;
    private int Status;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getImageID() {
        return ImageID;
    }

    public void setImageID(int imageID) {
        ImageID = imageID;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }
}
