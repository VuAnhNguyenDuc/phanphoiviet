package com.model;

/**
 * Created by Vu Anh Nguyen Duc on 6/26/2017.
 */
public class Introduction {
    private int ID;
    private String Title;
    private String Content;
    private int ImageID;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public int getImageID() {
        return ImageID;
    }

    public void setImageID(int imageID) {
        ImageID = imageID;
    }
}
