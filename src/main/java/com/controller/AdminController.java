package com.controller;

import com.form.*;
import com.hibernate.*;
import com.model.*;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Vu Anh Nguyen Duc on 6/18/2017.
 */
@Controller
public class AdminController {
    private static BrandHibernate brandHibernate = new BrandHibernate();
    private static ImageHibernate imageHibernate = new ImageHibernate();
    private static NewsHibernate newsHibernate = new NewsHibernate();
    private static ProductHibernate productHibernate = new ProductHibernate();
    private static UserHibernate userHibernate = new UserHibernate();
    private static PartnerHibernate partnerHibernate = new PartnerHibernate();
    private static FeedbackHibernate feedbackHibernate = new FeedbackHibernate();
    private static SlideImageHibernate slideImageHibernate = new SlideImageHibernate();

    private String convertToUTF8(String input) throws UnsupportedEncodingException {
        return new String (input.getBytes ("iso-8859-1"), "UTF-8");
    }

    private String  saveResource(HttpServletRequest request, MultipartFile multipartFile) throws IOException {
        BufferedImage src = ImageIO.read(new ByteArrayInputStream(multipartFile.getBytes()));

        String orgName = multipartFile.getOriginalFilename(); // background.jpg
        String extension = orgName.substring(orgName.lastIndexOf(".")+1);

        String bcd = multipartFile.getContentType(); // image/jpeg
        String fileType = multipartFile.getName(); // Image

        String uploadPath = request.getSession().getServletContext().getRealPath("/resources/images");
        File f = new File(uploadPath);
        if(!f.isDirectory()){
            f.mkdirs();
        }
        // Check if a file with the same name exist, if that happens, we will create another name of that file
        File checkFile = new File(uploadPath + "/" + orgName);
        if(checkFile.exists()){
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("HH-mm-ss_dd-MM-yyyy");
            orgName = orgName.substring(0, orgName.lastIndexOf(".")) + "_" + sdf.format(cal.getTime()) + "." + extension;
        }

        ImageIO.write(src,extension, new File(uploadPath + "/" + orgName));

        return orgName;
    }

    private Integer saveImageAndGetID(HttpServletRequest request, MultipartFile multipartFile) throws IOException {
        String fileName = saveResource(request,multipartFile);

        Image newImage = new Image();
        newImage.setPath("/images/" + fileName);

        Integer newID = imageHibernate.saveImage(newImage);
        return newID;
    }

    private String renameImage(String path, Image newImage, String newName){
        String oldName = newImage.getPath().substring(newImage.getPath().lastIndexOf("/")+1);
        String extension = oldName.substring(oldName.lastIndexOf(".") + 1);

        File oldFile = new File(path+ "/" + oldName);
        File newFile = new File(path + "/" + newName+"."+extension);

        if(oldFile.renameTo(newFile)){
            return newName+"."+extension;
        } else{
            return "failed";
        }
    }

    private void updateImage(HttpServletRequest request,String newImageName, int imageID){
        String uploadPath = request.getSession().getServletContext().getRealPath("/resources/images");

        Image image = imageHibernate.getImage(imageID);
        image.setPath("/images/" + renameImage(uploadPath,image, newImageName));
        imageHibernate.updateImage(image);
    }

    private boolean isLogin(HttpSession session){
        return !(session.getAttribute("username") == null || session.getAttribute("username").equals(""));
    }

    /* LOGIN */
    @RequestMapping(value = {"/admin","/admin/login"}, method = RequestMethod.GET)
    public String loginPage(ModelMap model){
        model.addAttribute("loginForm",new LoginForm());
        return "admin/login";}

    @RequestMapping(value = {"/admin/login"}, method = RequestMethod.POST)
    public String loginPostPage(HttpServletRequest request, HttpServletResponse response, @Valid LoginForm loginForm, BindingResult result, ModelMap model){
        if(result.hasErrors()){
            return "admin/login";
        } else{
            if(userHibernate.getAdmin(loginForm.getUsername(),loginForm.getPassword()) != null){
                HttpSession httpSession = request.getSession();
                httpSession.setAttribute("username",loginForm.getUsername());
                return "redirect:/admin/home";
            } else{
                model.addAttribute("result","Tên đăng nhập hoặc mật khẩu không đúng");
                return "admin/login";
            }
        }
    }

    @RequestMapping(value = "/admin/signout", method = RequestMethod.GET)
    public String signOutPage(HttpServletRequest request, HttpServletResponse response){
        HttpSession httpSession = request.getSession();
        httpSession.invalidate();
        return "redirect:/admin/login";
    }



    @RequestMapping(value = {"/admin/home"}, method = RequestMethod.GET)
    public String homePage(HttpServletRequest request,ModelMap model){
        if(!isLogin(request.getSession())){
            return "redirect:/admin";
        }
        model.addAttribute("pageName","home.jsp");
        return "admin/home";}

    /* INSERT, EDIT BRAND */
    private String insertUpdateBrand(HttpServletRequest request,InsertEditBrandForm insertEditBrandForm, int id, boolean changeImage) throws IOException {
        Brand newBrand = new Brand();
        if(id == 0){    // insert
            Integer newID = saveImageAndGetID(request,insertEditBrandForm.getPath());

            newBrand.setName(insertEditBrandForm.getName());
            newBrand.setOrigin(insertEditBrandForm.getOrigin());
            newBrand.setImageID(newID);
            brandHibernate.saveBrand(newBrand);

            String newImageName = "brand_"+newBrand.getID();
            updateImage(request,newImageName,newID);
        } else{
            newBrand = brandHibernate.getBrand(id);
            newBrand.setName(insertEditBrandForm.getName());
            newBrand.setOrigin(insertEditBrandForm.getOrigin());

            if(changeImage){
                String fileName = saveResource(request,insertEditBrandForm.getPath());

                Image newImage = new Image();
                newImage.setPath("/images/" + fileName);

                Integer newID = imageHibernate.saveImage(newImage);
                newBrand.setImageID(newID);

                String newImageName = "brand_"+newBrand.getID();
                updateImage(request,newImageName,newID);
            }
            brandHibernate.updateEntity(newBrand);
        }
        return "redirect:/admin/brand";
    }

    @RequestMapping(value = {"/admin/brand"}, method = RequestMethod.GET)
    public String brandPage(HttpServletRequest request,ModelMap model){
        if(!isLogin(request.getSession())){
            return "redirect:/admin";
        }
        model.addAttribute("pageName","brand.jsp");
        List<BrandView> brandViews = brandHibernate.getAllBrands();
        model.addAttribute("brandList",brandViews);
        return "admin/home";}

    @RequestMapping(value = {"/admin/brand/insert"}, method = RequestMethod.GET)
    public String brandInsertPage(HttpServletRequest request,ModelMap model){
        if(!isLogin(request.getSession())){
            return "redirect:/admin";
        }
        model.addAttribute("order","insert");
        model.addAttribute("insertEditBrandForm", new InsertEditBrandForm());
        return "admin/insert-edit-brand";}

    @RequestMapping(value = {"/admin/brand/insert"}, method = RequestMethod.POST)
    public String brandInsertPostPage(HttpServletRequest request, HttpServletResponse response, @Valid InsertEditBrandForm insertEditBrandForm, BindingResult result, ModelMap model){
        if(result.hasErrors()){
            model.addAttribute("order","insert");
            return "admin/insert-edit-brand";
        } else{
            if(insertEditBrandForm.getPath().isEmpty()){
                model.addAttribute("Error", "Bạn chưa chèn hình ảnh vào");
                model.addAttribute("order","insert");
                return "admin/insert-edit-brand";
            }
            try {
                return insertUpdateBrand(request,insertEditBrandForm,0,false);
            } catch (Exception e) {
                model.addAttribute("Error",e.getMessage());
                return "admin/error-page";
            }
        }
    }

    @RequestMapping(value = {"/admin/brand/update"}, method = RequestMethod.GET, params = "id")
    public String brandUpdatePage(@RequestParam("id") Integer id, HttpServletRequest request, ModelMap model){
        if(!isLogin(request.getSession())){
            return "redirect:/admin";
        }
        InsertEditBrandForm insertEditBrandForm = new InsertEditBrandForm();
        Brand brand = brandHibernate.getBrand(id);
        insertEditBrandForm.setName(brand.getName());
        insertEditBrandForm.setOrigin(brand.getOrigin());

        String Path = imageHibernate.getImage(brand.getImageID()).getPath();

        model.addAttribute("ImageLink", Path);
        model.addAttribute("insertEditBrandForm", insertEditBrandForm);
        return "admin/Brand/updateBrand";}

    @RequestMapping(value = {"/admin/brand/update"}, method = RequestMethod.POST, params = "id")
    public String brandUpdatePostPage(@RequestParam("id") Integer id, HttpServletRequest request, HttpServletResponse response, @Valid InsertEditBrandForm insertEditBrandForm, BindingResult result, ModelMap model){
        if(result.hasErrors()){
            return "admin/Brand/updateBrand";
        } else{
            try {
                if(insertEditBrandForm.getPath().isEmpty()){
                    return insertUpdateBrand(request,insertEditBrandForm,id,false);
                }
                return insertUpdateBrand(request,insertEditBrandForm,id,true);
            } catch (Exception e) {
                model.addAttribute("Error",e.getMessage());
                return "admin/error-page";
            }
        }
    }


    /* INSERT, EDIT PRODUCT */
    @RequestMapping(value = {"/admin/product"}, method = RequestMethod.GET)
    public String productPage(HttpServletRequest request,ModelMap model){
        if(!isLogin(request.getSession())){
            return "redirect:/admin";
        }
        model.addAttribute("pageName","product.jsp");
        List<ProductView> productViews = productHibernate.getAllProduct();
        model.addAttribute("productList",productViews);
        return "admin/home";}

    private String insertUpdateProduct(HttpServletRequest request,InsertEditProductForm insertEditProductForm, int id, boolean changeImage) throws IOException{
        Product newProduct = new Product();
        if(id == 0){
            Integer newID = saveImageAndGetID(request, insertEditProductForm.getPath());

            newProduct.setName(insertEditProductForm.getName());
            newProduct.setWeight(insertEditProductForm.getWeight());
            newProduct.setStatus(insertEditProductForm.getStatus());
            newProduct.setBrandID(insertEditProductForm.getBrandID());
            newProduct.setImageID(newID);
            productHibernate.saveProduct(newProduct);

            String newImageName = "product_"+newProduct.getID();
            updateImage(request,newImageName,newID);
        } else{
            newProduct = productHibernate.getProduct(id);
            newProduct.setName(insertEditProductForm.getName());
            newProduct.setBrandID(insertEditProductForm.getBrandID());
            newProduct.setWeight(insertEditProductForm.getWeight());
            newProduct.setStatus(insertEditProductForm.getStatus());

            if(changeImage){
                Integer newID = saveImageAndGetID(request, insertEditProductForm.getPath());
                newProduct.setImageID(newID);
                String newImageName = "product_"+newProduct.getID();
                updateImage(request,newImageName,newID);
            }
            productHibernate.updateEntity(newProduct);
        }
        return "redirect:/admin/product";
    }

    @RequestMapping(value = {"/admin/product/insert"}, method = RequestMethod.GET)
    public String productInsertPage(HttpServletRequest request,ModelMap model){
        if(!isLogin(request.getSession())){
            return "redirect:/admin";
        }
        model.addAttribute("order","insert");
        model.addAttribute("insertEditProductForm", new InsertEditProductForm());

        List<BrandView> brandViews = brandHibernate.getAllBrands();
        model.addAttribute("brandList",brandViews);

        return "admin/insert-edit-product";}

    @RequestMapping(value = {"/admin/product/insert"}, method = RequestMethod.POST)
    public String productInsertPostPage(HttpServletRequest request, HttpServletResponse response, @Valid InsertEditProductForm insertEditProductForm, BindingResult result, ModelMap model){
        if(result.hasErrors()){
            model.addAttribute("order","insert");
            return "admin/insert-edit-product";
        } else{
            if(insertEditProductForm.getPath().isEmpty()){
                model.addAttribute("Error", "Bạn chưa chèn hình ảnh vào");
                model.addAttribute("order","insert");
                return "admin/insert-edit-product";
            }
            try {
                return insertUpdateProduct(request,insertEditProductForm,0,false);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "redirect:/admin/product";
        }
    }

    @RequestMapping(value = {"/admin/product/update"}, method = RequestMethod.GET, params = "id")
    public String productUpdatePage(@RequestParam("id") Integer id, HttpServletRequest request, ModelMap model){
        if(!isLogin(request.getSession())){
            return "redirect:/admin";
        }
        InsertEditProductForm insertEditProductForm = new InsertEditProductForm();
        Product product = productHibernate.getProduct(id);
        insertEditProductForm.setName(product.getName());
        insertEditProductForm.setWeight(product.getWeight());
        insertEditProductForm.setBrandID(product.getBrandID());
        insertEditProductForm.setStatus(product.getStatus());

        String Path = imageHibernate.getImage(product.getImageID()).getPath();

        model.addAttribute("ImageLink", Path);
        model.addAttribute("ID", id);
        model.addAttribute("insertEditProductForm", insertEditProductForm);

        List<BrandView> brandViews = brandHibernate.getAllBrands();
        model.addAttribute("brandList",brandViews);

        return "admin/Product/updateProduct";}

    @RequestMapping(value = {"/admin/product/update"}, method = RequestMethod.POST, params = "id")
    public String productUpdatePostPage(@RequestParam("id") Integer id, HttpServletRequest request, HttpServletResponse response, @Valid InsertEditProductForm insertEditProductForm, BindingResult result, ModelMap model){
        if(result.hasErrors()){
            return "admin/Product/updateProduct";
        } else{
            try {
                if(insertEditProductForm.getPath().isEmpty()){
                    return insertUpdateProduct(request,insertEditProductForm,id,false);
                }
                return insertUpdateProduct(request,insertEditProductForm,id,true);
            } catch (Exception e) {
                model.addAttribute("Error",e.getMessage());
                return "admin/error-page";
            }
        }
    }


    /* GET THE CONTENT OF A NEWS (READABLE ONLY)*/
    @RequestMapping(value = {"/admin/news/content"}, method = RequestMethod.GET, params = "id")
    public String getContentPage(@RequestParam("id") Integer id, HttpServletRequest request, ModelMap model){
        if(!isLogin(request.getSession())){
            return "redirect:/admin";
        }
        News news = newsHibernate.getNews(id);
        model.addAttribute("Content", news.getContent());
        model.addAttribute("pageName","get-news-content.jsp");
        return "/admin/home";
    }

    /* INSERT, EDIT NEWS */
    @RequestMapping(value = {"/admin/news"}, method = RequestMethod.GET)
    public String newsPage(HttpServletRequest request,ModelMap model){
        if(!isLogin(request.getSession())){
            return "redirect:/admin";
        }
        model.addAttribute("pageName","news.jsp");
        List<NewsView> newsViews = newsHibernate.getAllNews();
        model.addAttribute("newsList",newsViews);
        return "admin/home";}

    private String insertUpdateNews(HttpServletRequest request, InsertEditNewsForm insertEditNewsForm, int id, boolean changeImage) throws IOException {
        News news = new News();
        if(id == 0){
            Integer newID = saveImageAndGetID(request, insertEditNewsForm.getPath());

            news.setContent(insertEditNewsForm.getContent());
            news.setStatus(insertEditNewsForm.getStatus());
            news.setTitle(insertEditNewsForm.getTitle());
            news.setImageID(newID);
            newsHibernate.saveNews(news);

            String newImageName = "news_"+news.getID();
            updateImage(request,newImageName,newID);
        } else{
            news = newsHibernate.getNews(id);
            news.setContent(insertEditNewsForm.getContent());
            news.setStatus(insertEditNewsForm.getStatus());
            news.setTitle(insertEditNewsForm.getTitle());

            if(changeImage){
                Integer newID = saveImageAndGetID(request,insertEditNewsForm.getPath());
                news.setImageID(newID);

                String newImageName = "news_"+news.getID();
                updateImage(request,newImageName,newID);
            }
            newsHibernate.updateNews(news);
        }
        return "redirect:/admin/news";
    }

    @RequestMapping(value = {"/admin/news/insert"}, method = RequestMethod.GET)
    public String newsInsertPage(HttpServletRequest request, ModelMap model){
        if(!isLogin(request.getSession())){
            return "redirect:/admin";
        }
        model.addAttribute("order","insert");
        model.addAttribute("insertEditNewsForm", new InsertEditNewsForm());
        return "admin/insert-edit-news";}

    @RequestMapping(value = {"/admin/news/insert"}, method = RequestMethod.POST)
    public String newsInsertPostPage(HttpServletRequest request, HttpServletResponse response, @Valid InsertEditNewsForm insertEditNewsForm, BindingResult result, ModelMap model){
        if(result.hasErrors()){
            model.addAttribute("order","insert");
            return "admin/insert-edit-news";
        } else{
            if(insertEditNewsForm.getPath().isEmpty()){
                model.addAttribute("order","insert");
                model.addAttribute("Error", "Bạn chưa chèn hình ảnh vào");
                return "admin/insert-edit-news";
            }
            try {
                return insertUpdateNews(request,insertEditNewsForm,0,false);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "redirect:/admin/news";
        }
    }

    @RequestMapping(value = {"/admin/news/update"}, method = RequestMethod.GET, params = "id")
    public String newsUpdatePage(@RequestParam("id") Integer id, HttpServletRequest request, ModelMap model){
        if(!isLogin(request.getSession())){
            return "redirect:/admin";
        }
        InsertEditNewsForm insertEditNewsForm = new InsertEditNewsForm();
        News news = newsHibernate.getNews(id);
        insertEditNewsForm.setContent(news.getContent());
        insertEditNewsForm.setTitle(news.getTitle());
        insertEditNewsForm.setStatus(news.getStatus());

        String Path = imageHibernate.getImage(news.getImageID()).getPath();

        model.addAttribute("ImageLink", Path);
        model.addAttribute("insertEditNewsForm", insertEditNewsForm);
        return "admin/News/updateNews";}

    @RequestMapping(value = {"/admin/news/update"}, method = RequestMethod.POST, params = "id")
    public String newsUpdatePostPage(@RequestParam("id") Integer id, HttpServletRequest request, HttpServletResponse response, @Valid InsertEditNewsForm insertEditNewsForm, BindingResult result, ModelMap model){
        if(result.hasErrors()){
            return "admin/News/updateNews";
        } else{
            try {
                if(insertEditNewsForm.getPath().isEmpty()){
                    return insertUpdateNews(request,insertEditNewsForm,id,false);
                }
                return insertUpdateNews(request,insertEditNewsForm,id,true);
            } catch (Exception e) {
                model.addAttribute("Error",e.getMessage());
                return "admin/error-page";
            }
        }
    }




    /* INSERT, EDIT USERS */
    @RequestMapping(value = {"/admin/users"}, method = RequestMethod.GET)
    public String usersPage(HttpServletRequest request,ModelMap model){
        if(!isLogin(request.getSession())){
            return "redirect:/admin";
        }
        model.addAttribute("pageName","users.jsp");
        List<User> users = userHibernate.getAllUser();
        model.addAttribute("userList",users);
        return "admin/home";}

    private String insertUpdateUser(InsertEditUserForm insertEditUserForm, int id){
        User newUser = new User();
        if(id != 0){
            newUser = userHibernate.getUser(id);
        }

        newUser.setStatus(insertEditUserForm.getStatus());
        newUser.setType(insertEditUserForm.getType());
        newUser.setUsername(insertEditUserForm.getUsername());
        newUser.setPassword(insertEditUserForm.getPassword());

        if(id == 0){
            userHibernate.saveUser(newUser);
        } else{
            userHibernate.updateUser(newUser);}
        return "redirect:/admin/users";
    }

    @RequestMapping(value = {"/admin/users/insert"}, method = RequestMethod.GET)
    public String usersInsertPage(HttpServletRequest request,ModelMap model){
        if(!isLogin(request.getSession())){return "redirect:/admin";}
        model.addAttribute("order","insert");
        model.addAttribute("insertEditUserForm", new InsertEditUserForm());
        return "admin/insert-edit-users";}

    @RequestMapping(value = {"/admin/users/insert"}, method = RequestMethod.POST)
    public String usersInsertPostPage(HttpServletRequest request, HttpServletResponse response, @Valid InsertEditUserForm insertEditUserForm, BindingResult result, ModelMap model){
        if(result.hasErrors()){
            model.addAttribute("order","insert");
            return "admin/insert-edit-users";
        } else{
            return insertUpdateUser(insertEditUserForm,0);
        }
    }

    @RequestMapping(value = {"/admin/users/update"}, method = RequestMethod.GET, params = "id")
    public String usersUpdatePage(@RequestParam("id") Integer id, HttpServletRequest request, ModelMap model){
        if(!isLogin(request.getSession())){return "redirect:/admin";}
        InsertEditUserForm insertEditUserForm = new InsertEditUserForm();
        User user = userHibernate.getUser(id);
        insertEditUserForm.setUsername(user.getUsername());
        insertEditUserForm.setPassword(user.getPassword());
        insertEditUserForm.setStatus(user.getStatus());
        insertEditUserForm.setType(user.getType());

        model.addAttribute("insertEditUserForm", insertEditUserForm);
        return "admin/User/updateUser";}

    @RequestMapping(value = {"/admin/users/update"}, method = RequestMethod.POST, params = "id")
    public String usersUpdatePostPage(@RequestParam("id") Integer id, HttpServletRequest request, HttpServletResponse response, @Valid InsertEditUserForm insertEditUserForm, BindingResult result, ModelMap model){
        if(result.hasErrors()){
            return "admin/User/updateUser";
        } else{
            return insertUpdateUser(insertEditUserForm,id);
        }
    }

    /* INSERT, EDIT PARTNER */
    @RequestMapping(value = {"/admin/partners"}, method = RequestMethod.GET)
    public String partnerPage(HttpServletRequest request,ModelMap model){
        if(!isLogin(request.getSession())){
            return "redirect:/admin";
        }
        model.addAttribute("pageName","partners.jsp");
        List<PartnerView> partnerViews = partnerHibernate.getAllPartner();
        model.addAttribute("partnerList",partnerViews);
        return "admin/home";}

    private String insertUpdatePartner(HttpServletRequest request, InsertEditPartnerForm insertEditPartnerForm, int id, boolean changeImage) throws IOException {
        Partner partner = new Partner();
        if(id == 0){
            Integer newID = saveImageAndGetID(request, insertEditPartnerForm.getImageFile());

            partner.setName(insertEditPartnerForm.getName());
            partner.setStatus(insertEditPartnerForm.getStatus());
            partner.setAddress(insertEditPartnerForm.getAddress());
            partner.setImageID(newID);
            partnerHibernate.savePartner(partner);

            String newImageName = "partner_"+partner.getID();
            updateImage(request,newImageName,newID);
        } else{
            partner = partnerHibernate.getPartner(id);
            partner.setName(insertEditPartnerForm.getName());
            partner.setStatus(insertEditPartnerForm.getStatus());
            partner.setAddress(insertEditPartnerForm.getAddress());

            if(changeImage){
                Integer newID = saveImageAndGetID(request,insertEditPartnerForm.getImageFile());
                partner.setImageID(newID);

                String newImageName = "partner_"+partner.getID();
                updateImage(request,newImageName,newID);
            }
            partnerHibernate.updatePartner(partner);
        }
        return "redirect:/admin/partners";
    }

    @RequestMapping(value = {"/admin/partners/insert"}, method = RequestMethod.GET)
    public String partnerInsertPage(HttpServletRequest request, ModelMap model){
        if(!isLogin(request.getSession())){
            return "redirect:/admin";
        }
        model.addAttribute("order","insert");
        model.addAttribute("insertEditPartnerForm", new InsertEditPartnerForm());
        return "admin/Partner/insertPartner";}

    @RequestMapping(value = {"/admin/partners/insert"}, method = RequestMethod.POST)
    public String partnerInsertPostPage(HttpServletRequest request, HttpServletResponse response, @Valid InsertEditPartnerForm insertEditPartnerForm, BindingResult result, ModelMap model){
        if(result.hasErrors()){
            model.addAttribute("order","insert");
            return "admin/Partner/insertPartner";
        } else{
            if(insertEditPartnerForm.getImageFile().isEmpty()){
                model.addAttribute("order","insert");
                model.addAttribute("Error", "Bạn chưa chèn hình ảnh vào");
                return "admin/Partner/insertPartner";
            }
            try {
                return insertUpdatePartner(request,insertEditPartnerForm,0,false);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "redirect:/admin/partner";
        }
    }

    @RequestMapping(value = {"/admin/partners/update"}, method = RequestMethod.GET, params = "id")
    public String partnerUpdatePage(@RequestParam("id") Integer id, HttpServletRequest request, ModelMap model){
        if(!isLogin(request.getSession())){
            return "redirect:/admin";
        }
        InsertEditPartnerForm insertEditPartnerForm = new InsertEditPartnerForm();
        Partner partner = partnerHibernate.getPartner(id);
        insertEditPartnerForm.setAddress(partner.getAddress());
        insertEditPartnerForm.setName(partner.getName());
        insertEditPartnerForm.setStatus(partner.getStatus());

        String Path = imageHibernate.getImage(partner.getImageID()).getPath();

        model.addAttribute("ImageLink", Path);
        model.addAttribute("insertEditPartnerForm", insertEditPartnerForm);
        return "admin/Partner/updatePartner";}

    @RequestMapping(value = {"/admin/partners/update"}, method = RequestMethod.POST, params = "id")
    public String partnerUpdatePostPage(@RequestParam("id") Integer id, HttpServletRequest request, HttpServletResponse response, @Valid InsertEditPartnerForm insertEditPartnerForm, BindingResult result, ModelMap model){
        if(result.hasErrors()){
            return "admin/Partner/updatePartner";
        } else{
            try {
                if(insertEditPartnerForm.getImageFile().isEmpty()){
                    return insertUpdatePartner(request,insertEditPartnerForm,id,false);
                }
                return insertUpdatePartner(request,insertEditPartnerForm,id,true);
            } catch (Exception e) {
                model.addAttribute("Error",e.getMessage());
                return "admin/error-page";
            }
        }
    }

    /* VIEW FEEDBACKS */
    @RequestMapping(value = "/admin/feedback", method = RequestMethod.GET)
    public String feedbackPage(HttpServletRequest request,ModelMap model){
        if(!isLogin(request.getSession())){return "redirect:/admin";}
        model.addAttribute("pageName","feedback.jsp");
        List<Feedback> feedbackList = feedbackHibernate.getAllFeedback();
        model.addAttribute("feedbackList",feedbackList);
        return "admin/home";}

    /* INSERT IMAGE */
    @RequestMapping(value = {"/admin/image"}, method = RequestMethod.GET)
    public String imagePage(HttpServletRequest request,ModelMap model){
        if(!isLogin(request.getSession())){return "redirect:/admin";}
        model.addAttribute("pageName","image.jsp");
        List<Image> images = imageHibernate.getAllImages();
        model.addAttribute("imageList",images);
        return "admin/home";}

    @RequestMapping(value = {"/admin/image/insert"}, method = RequestMethod.GET)
    public String imageInsertPage(HttpServletRequest request,ModelMap model){
        if(!isLogin(request.getSession())){return "redirect:/admin";}
        model.addAttribute("order","insert");
        model.addAttribute("insertEditImageForm", new InsertEditImageForm());
        return "admin/insert-edit-image";}

    @RequestMapping(value = {"/admin/image/insert"}, method = RequestMethod.POST)
    public String imageInsertPostPage(HttpServletRequest request, HttpServletResponse response, @Valid InsertEditImageForm insertEditImageForm, BindingResult result, ModelMap model){
        if(result.hasErrors()){
            model.addAttribute("order","insert");
            return "admin/insert-edit-image";
        } else{
            if(insertEditImageForm.getImage().isEmpty()){
                model.addAttribute("order","insert");
                model.addAttribute("Error", "Bạn chưa chèn hình ảnh vào");
                return "admin/insert-edit-image";
            }
            try {
                String fileName = saveResource(request,insertEditImageForm.getImage());

                Image newImage = new Image();
                newImage.setPath("/images/" + fileName);

                int newID = imageHibernate.saveImage(newImage);

                String newImageName = "image_"+newImage.getID();
                updateImage(request,newImageName,newID);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "redirect:/admin/image";
        }
    }







    /* INSERT, EDIT SLIDE IMAGE*/
    @RequestMapping(value = {"/admin/slideImage"}, method = RequestMethod.GET)
    public String slidePage(HttpServletRequest request, ModelMap model){
        if(!isLogin(request.getSession())){
            return "redirect:/admin";
        }
        model.addAttribute("pageName","slideImage.jsp");
        List<SlideImageView> slideImageViews = slideImageHibernate.getAllSlideImages();
        model.addAttribute("slideImageList",slideImageViews);
        return "admin/home";}

    private String insertUpdateSlideImage(HttpServletRequest request, InsertEditSlideImageForm insertEditSlideImageForm, int id, boolean changeImage) throws IOException {
        SlideImage slideImage = new SlideImage();
        if(id == 0){
            Integer newID = saveImageAndGetID(request, insertEditSlideImageForm.getPath());
            slideImage.setStatus(insertEditSlideImageForm.getStatus());
            slideImage.setImageID(newID);
            slideImageHibernate.saveSlideImage(slideImage);

            String newImageName = "slideImage_"+slideImage.getID();
            updateImage(request,newImageName,newID);
        } else{
            slideImage = slideImageHibernate.getSlideImage(id);
            slideImage.setStatus(insertEditSlideImageForm.getStatus());
            if(changeImage){
                Integer newID = saveImageAndGetID(request,insertEditSlideImageForm.getPath());
                slideImage.setImageID(newID);

                String newImageName = "slideImage_"+slideImage.getID();
                updateImage(request,newImageName,newID);
            }
            slideImageHibernate.updateSlideImage(slideImage);
        }
        return "redirect:/admin/slideImage";
    }

    @RequestMapping(value = {"/admin/slideImage/insert"}, method = RequestMethod.GET)
    public String slideImageInsertPage(HttpServletRequest request, ModelMap model){
        if(!isLogin(request.getSession())){
            return "redirect:/admin";
        }
        model.addAttribute("order","insert");
        model.addAttribute("insertEditSlideImageForm", new InsertEditSlideImageForm());
        return "admin/SlideImage/insertSlideImage";}

    @RequestMapping(value = {"/admin/slideImage/insert"}, method = RequestMethod.POST)
    public String slideImageInsertPostPage(HttpServletRequest request, HttpServletResponse response, @Valid InsertEditSlideImageForm insertEditSlideImageForm, BindingResult result, ModelMap model){
        if(result.hasErrors()){
            model.addAttribute("order","insert");
            return "admin/SlideImage/insertSlideImage";
        } else{
            if(insertEditSlideImageForm.getPath().isEmpty()){
                model.addAttribute("order","insert");
                model.addAttribute("Error", "Bạn chưa chèn hình ảnh vào");
                return "admin/SlideImage/insertSlideImage";
            }
            try {
                return insertUpdateSlideImage(request,insertEditSlideImageForm,0,false);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "redirect:/admin/slideImage";
        }
    }

    @RequestMapping(value = {"/admin/slideImage/update"}, method = RequestMethod.GET, params = "id")
    public String slideImageUpdatePage(@RequestParam("id") Integer id, HttpServletRequest request, ModelMap model){
        if(!isLogin(request.getSession())){
            return "redirect:/admin";
        }

        InsertEditSlideImageForm insertEditSlideImageForm = new InsertEditSlideImageForm();
        SlideImage slideImage = slideImageHibernate.getSlideImage(id);
        insertEditSlideImageForm.setStatus(slideImage.getStatus());
        String Path = imageHibernate.getImage(slideImage.getImageID()).getPath();

        model.addAttribute("ImageLink", Path);
        model.addAttribute("insertEditSlideImageForm", insertEditSlideImageForm);
        return "admin/SlideImage/updateSlideImage";}

    @RequestMapping(value = {"/admin/slideImage/update"}, method = RequestMethod.POST, params = "id")
    public String slideImageUpdatePostPage(@RequestParam("id") Integer id, HttpServletRequest request, HttpServletResponse response, @Valid InsertEditSlideImageForm insertEditSlideImageForm, BindingResult result, ModelMap model){
        if(result.hasErrors()){
            return "admin/SlideImage/updateSlideImage";
        } else{
            try {
                if(insertEditSlideImageForm.getPath().isEmpty()){
                    return insertUpdateSlideImage(request,insertEditSlideImageForm,id,false);
                }
                return insertUpdateSlideImage(request,insertEditSlideImageForm,id,true);
            } catch (Exception e) {
                model.addAttribute("Error",e.getMessage());
                return "admin/error-page";
            }
        }
    }
}
