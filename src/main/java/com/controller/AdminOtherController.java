package com.controller;

import com.form.EditOtherForm;
import com.hibernate.OtherHibernate;
import com.model.Other;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;

/**
 * Created by Vu Anh Nguyen Duc on 6/21/2017.
 */
@Controller
public class AdminOtherController {
    private static OtherHibernate otherHibernate = new OtherHibernate();

    private String removePreTag(String input){
        return input.replace("<pre>","").replace("</pre>","");
    }

    private String convertToUTF8(String input) throws UnsupportedEncodingException {
        return new String (input.getBytes ("iso-8859-1"), "UTF-8");
    }

    @RequestMapping(value = {"/admin/other"}, method = RequestMethod.GET, params = "type")
    public String otherGetPage(@RequestParam("type") String type, ModelMap model){
        Other entity = otherHibernate.getOther(type);
        EditOtherForm editOtherForm = new EditOtherForm();
        editOtherForm.setContent((otherHibernate.getOther(type).getContent() == null)? " " : otherHibernate.getOther(type).getContent());
        model.addAttribute("pageName", type + ".jsp");
        model.addAttribute("editOtherForm", editOtherForm);
        return "/admin/Other/general";
    }

    @RequestMapping(value = {"/admin/other"}, method = RequestMethod.POST, params = "type")
    public String otherPostPage(@RequestParam("type") String type, HttpServletRequest request, HttpServletResponse response, @Valid EditOtherForm editOtherForm, BindingResult result, ModelMap model){
        if(result.hasErrors()){
            return "/admin/Other/general";
        } else{
            try {
                editOtherForm.setContent(removePreTag(editOtherForm.getContent()));
                editOtherForm.setContent(convertToUTF8(editOtherForm.getContent()));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String Content = editOtherForm.getContent();
            Other entity = otherHibernate.getOther(type);
            entity.setContent(Content);
            otherHibernate.updateEntity(entity);
            model.addAttribute("pageName","home.jsp");
            return "redirect:/admin/home";
        }
    }
}
