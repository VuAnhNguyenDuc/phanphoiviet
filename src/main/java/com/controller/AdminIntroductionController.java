package com.controller;

import com.form.InsertEditIntroductionForm;
import com.hibernate.ImageHibernate;
import com.hibernate.IntroductionHibernate;
import com.model.Image;
import com.model.Introduction;
import com.model.IntroductionView;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Vu Anh Nguyen Duc on 6/26/2017.
 */
@Controller
public class AdminIntroductionController {
    private static ImageHibernate imageHibernate = new ImageHibernate();
    private static IntroductionHibernate introductionHibernate = new IntroductionHibernate();

    private String convertToUTF8(String input) throws UnsupportedEncodingException {
        return new String (input.getBytes ("iso-8859-1"), "UTF-8");
    }

    private String  saveResource(HttpServletRequest request, MultipartFile multipartFile) throws IOException {
        BufferedImage src = ImageIO.read(new ByteArrayInputStream(multipartFile.getBytes()));

        String orgName = multipartFile.getOriginalFilename(); // background.jpg
        String extension = orgName.substring(orgName.lastIndexOf(".")+1);

        String bcd = multipartFile.getContentType(); // image/jpeg
        String fileType = multipartFile.getName(); // Image

        String uploadPath = request.getSession().getServletContext().getRealPath("/resources/images");
        File f = new File(uploadPath);
        if(!f.isDirectory()){
            f.mkdirs();
        }
        // Check if a file with the same name exist, if that happens, we will create another name of that file
        File checkFile = new File(uploadPath + "/" + orgName);
        if(checkFile.exists()){
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("HH-mm-ss_dd-MM-yyyy");
            orgName = orgName.substring(0, orgName.lastIndexOf(".")) + "_" + sdf.format(cal.getTime()) + "." + extension;
        }

        ImageIO.write(src,extension, new File(uploadPath + "/" + orgName));

        return orgName;
    }

    private Integer saveImageAndGetID(HttpServletRequest request, MultipartFile multipartFile) throws IOException {
        String fileName = saveResource(request,multipartFile);

        Image newImage = new Image();
        newImage.setPath("/images/" + fileName);

        Integer newID = imageHibernate.saveImage(newImage);
        return newID;
    }

    private String renameImage(String path, Image newImage, String newName){
        String oldName = newImage.getPath().substring(newImage.getPath().lastIndexOf("/")+1);
        String extension = oldName.substring(oldName.lastIndexOf(".") + 1);

        File oldFile = new File(path+ "/" + oldName);
        File newFile = new File(path + "/" + newName+"."+extension);

        if(oldFile.renameTo(newFile)){
            return newName+"."+extension;
        } else{
            return "failed";
        }
    }

    private void updateImage(HttpServletRequest request,String newImageName, int imageID){
        String uploadPath = request.getSession().getServletContext().getRealPath("/resources/images");

        Image image = imageHibernate.getImage(imageID);
        image.setPath("/images/" + renameImage(uploadPath,image, newImageName));
        imageHibernate.updateImage(image);
    }

    private boolean isLogin(HttpSession session){
        return !(session.getAttribute("username") == null || session.getAttribute("username").equals(""));
    }

    /* GET THE CONTENT OF A NEWS (READABLE ONLY)*/
    @RequestMapping(value = {"/admin/introduction/content"}, method = RequestMethod.GET, params = "id")
    public String getContentPage(@RequestParam("id") Integer id, HttpServletRequest request, ModelMap model){
        if(!isLogin(request.getSession())){
            return "redirect:/admin";
        }
        Introduction introduction = introductionHibernate.getIntroduction(id);
        model.addAttribute("Content", introduction.getContent());
        model.addAttribute("pageName","get-introduction-content.jsp");
        return "/admin/home";
    }

    private String insertUpdateIntroduction(HttpServletRequest request, InsertEditIntroductionForm insertEditIntroductionForm, int id, boolean changeImage) throws IOException {
        Introduction newIntroduction = new Introduction();
        if(id == 0){    // insert
            Integer newID = saveImageAndGetID(request,insertEditIntroductionForm.getPath());

            newIntroduction.setTitle(insertEditIntroductionForm.getTitle());
            newIntroduction.setContent(insertEditIntroductionForm.getContent());
            newIntroduction.setImageID(newID);
            introductionHibernate.saveIntroduction(newIntroduction);

            String newImageName = "introduction_"+newIntroduction.getID();
            updateImage(request,newImageName,newID);
        } else{
            newIntroduction = introductionHibernate.getIntroduction(id);
            newIntroduction.setContent(insertEditIntroductionForm.getContent());
            newIntroduction.setTitle(insertEditIntroductionForm.getTitle());

            if(changeImage){
                String fileName = saveResource(request,insertEditIntroductionForm.getPath());

                Image newImage = new Image();
                newImage.setPath("/images/" + fileName);

                Integer newID = imageHibernate.saveImage(newImage);
                newIntroduction.setImageID(newID);

                String newImageName = "introduction_"+newIntroduction.getID();
                updateImage(request,newImageName,newID);
            }
            introductionHibernate.updateEntity(newIntroduction);
        }
        return "redirect:/admin/introduction";
    }

    @RequestMapping(value = {"/admin/introduction"}, method = RequestMethod.GET)
    public String introductionPage(HttpServletRequest request,ModelMap model){
        if(!isLogin(request.getSession())){
            return "redirect:/admin";
        }
        model.addAttribute("pageName","introduction.jsp");
        List<IntroductionView> introductionViews = introductionHibernate.getAllIntroductions();
        model.addAttribute("introductionList",introductionViews);
        return "admin/home";}

    @RequestMapping(value = {"/admin/introduction/insert"}, method = RequestMethod.GET)
    public String introductionInsertPage(HttpServletRequest request,ModelMap model){
        if(!isLogin(request.getSession())){
            return "redirect:/admin";
        }
        model.addAttribute("order","insert");
        model.addAttribute("insertEditIntroductionForm", new InsertEditIntroductionForm());
        return "admin/Introduction/insertIntroduction";}

    @RequestMapping(value = {"/admin/introduction/insert"}, method = RequestMethod.POST)
    public String introductionInsertPostPage(HttpServletRequest request, HttpServletResponse response, @Valid InsertEditIntroductionForm insertEditIntroductionForm, BindingResult result, ModelMap model){
        if(result.hasErrors()){
            model.addAttribute("order","insert");
            return "admin/Introduction/insertIntroduction";
        } else{
            if(insertEditIntroductionForm.getPath().isEmpty()){
                model.addAttribute("Error", "Bạn chưa chèn hình ảnh vào");
                model.addAttribute("order","insert");
                return "admin/insert-edit-introduction";
            }
            try {
                return insertUpdateIntroduction(request,insertEditIntroductionForm,0,false);
            } catch (Exception e) {
                model.addAttribute("Error",e.getMessage());
                return "admin/error-page";
            }
        }
    }

    @RequestMapping(value = {"/admin/introduction/update"}, method = RequestMethod.GET, params = "id")
    public String introductionUpdatePage(@RequestParam("id") Integer id, HttpServletRequest request, ModelMap model){
        if(!isLogin(request.getSession())){
            return "redirect:/admin";
        }
        InsertEditIntroductionForm insertEditIntroductionForm = new InsertEditIntroductionForm();
        Introduction introduction = introductionHibernate.getIntroduction(id);
        insertEditIntroductionForm.setContent(introduction.getContent());
        insertEditIntroductionForm.setTitle(introduction.getTitle());

        String Path = imageHibernate.getImage(introduction.getImageID()).getPath();

        model.addAttribute("ImageLink", Path);
        model.addAttribute("insertEditIntroductionForm", insertEditIntroductionForm);
        return "admin/Introduction/updateIntroduction";}

    @RequestMapping(value = {"/admin/introduction/update"}, method = RequestMethod.POST, params = "id")
    public String introductionUpdatePostPage(@RequestParam("id") Integer id, HttpServletRequest request, HttpServletResponse response, @Valid InsertEditIntroductionForm insertEditIntroductionForm, BindingResult result, ModelMap model){
        if(result.hasErrors()){
            return "admin/Introduction/updateIntroduction";
        } else{
            try {
                if(insertEditIntroductionForm.getPath().isEmpty()){
                    return insertUpdateIntroduction(request,insertEditIntroductionForm,id,false);
                }
                return insertUpdateIntroduction(request,insertEditIntroductionForm,id,true);
            } catch (Exception e) {
                model.addAttribute("Error",e.getMessage());
                return "admin/error-page";
            }
        }
    }
}
