package com.controller;

import com.form.InsertFeedbackForm;
import com.hibernate.*;
import com.model.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by Vu Anh Nguyen Duc on 6/17/2017.
 */
@Controller
public class HomeController {
    private static OtherHibernate otherHibernate = new OtherHibernate();
    private static BrandHibernate brandHibernate = new BrandHibernate();
    private static ImageHibernate imageHibernate = new ImageHibernate();
    private static NewsHibernate newsHibernate = new NewsHibernate();
    private static ProductHibernate productHibernate = new ProductHibernate();
    private static PartnerHibernate partnerHibernate = new PartnerHibernate();
    private static FeedbackHibernate feedbackHibernate = new FeedbackHibernate();
    private static SlideImageHibernate slideImageHibernate = new SlideImageHibernate();
    private static IntroductionHibernate introductionHibernate = new IntroductionHibernate();

    private String removePreTag(String input){
        return input.replace("<pre>","").replace("</pre>","");
    }

    private String convertToUTF8(String input) throws UnsupportedEncodingException {
        return new String (input.getBytes ("iso-8859-1"), "UTF-8");
    }

    private void insertBrands(ModelMap model){
        List<BrandView> brandViews = brandHibernate.getAllBrands();
        model.addAttribute("brandList", brandViews);
    }

    private void insertProduct(ModelMap model){
        List<ProductView> productViews = productHibernate.showEnabledProducts();
        model.addAttribute("AllProducts",productViews);
    }

    private void insertNews(ModelMap model){
        List<NewsView> newsViews = newsHibernate.showEnabledNews();
        model.addAttribute("AllNews", newsViews);
    }

    private void insertPartners(ModelMap model){
        List<PartnerView> partnerViews = partnerHibernate.showEnabledPartner();
        model.addAttribute("AllPartners",partnerViews);
    }


    @RequestMapping(value = {"/","/home"}, method = RequestMethod.GET)
    public String homePage(ModelMap model){
        String Banner = otherHibernate.getOther("banner").getContent();
        String Footer = otherHibernate.getOther("footer").getContent();
        Banner = Banner.replace("<p>","").replace("</p>","");
        Banner = removePreTag(Banner);
        Footer = removePreTag(Footer);

        List<SlideImageView> slideImageList = slideImageHibernate.showEnabledSlideImages();
        List<ProductView> HotProducts = productHibernate.getHotProduct();
        insertBrands(model); insertProduct(model); insertNews(model);
        model.addAttribute("HotProducts", HotProducts);
        model.addAttribute("slideImageList", slideImageList);
        model.addAttribute("Banner",Banner);
        model.addAttribute("Footer",Footer);
        return "web/home";
    }

    @RequestMapping(value = {"/introduction"}, method = RequestMethod.GET)
    public String introductionPage(ModelMap model){
        String Banner = otherHibernate.getOther("banner").getContent();
        String Content = otherHibernate.getOther("introduction").getContent();
        String Footer = otherHibernate.getOther("footer").getContent();
        Banner = Banner.replace("<p>","").replace("</p>","");
        Banner = removePreTag(Banner); Content = removePreTag(Content); Footer = removePreTag(Footer);

        List<IntroductionView> introductionViews = introductionHibernate.getAllIntroductions();

        insertBrands(model);
        model.addAttribute("AllIntroduction",introductionViews);
        model.addAttribute("Banner",Banner);
        model.addAttribute("Footer",Footer);
        return "web/introduction";
    }

    @RequestMapping(value = {"/introduction-details"}, method=RequestMethod.GET, params = "id")
    public String introductionDetailPage(@RequestParam("id") Integer id, ModelMap model){
        String Banner = otherHibernate.getOther("banner").getContent();
        String Footer = otherHibernate.getOther("footer").getContent();
        Banner = Banner.replace("<p>","").replace("</p>","");
        Banner = removePreTag(Banner); Footer = removePreTag(Footer);

        Introduction introduction = introductionHibernate.getIntroduction(id);

        insertBrands(model);
        model.addAttribute("introduction", introduction);
        model.addAttribute("Banner",Banner);
        model.addAttribute("Footer",Footer);
        return "web/introduction-details";}

    @RequestMapping(value = {"/products"}, method = RequestMethod.GET)
    public String productPage(ModelMap model){
        String Banner = otherHibernate.getOther("banner").getContent();
        String Footer = otherHibernate.getOther("footer").getContent();
        Banner = Banner.replace("<p>","").replace("</p>","");
        Banner = removePreTag(Banner); Footer = removePreTag(Footer);

        insertBrands(model);
        model.addAttribute("Banner",Banner);
        model.addAttribute("Footer",Footer);
        return "web/products";
    }

    @RequestMapping(value = {"/product-details"}, method=RequestMethod.GET, params = "id")
    public String productDetailPage(@RequestParam("id") Integer id, ModelMap model){
        String Banner = otherHibernate.getOther("banner").getContent();
        String Footer = otherHibernate.getOther("footer").getContent();
        Banner = Banner.replace("<p>","").replace("</p>","");
        Banner = removePreTag(Banner); Footer = removePreTag(Footer);

        ProductView product = productHibernate.getProductView(id);
        String Origin = brandHibernate.getBrandByName(product.getBrandName()).getOrigin();
        insertBrands(model);
        model.addAttribute("Origin",Origin);
        model.addAttribute("Product",product);
        model.addAttribute("Banner",Banner);
        model.addAttribute("Footer",Footer);
        return "web/product-details";}

    @RequestMapping(value = {"/partners"}, method = RequestMethod.GET)
    public String partnersPage(ModelMap model){
        String Banner = otherHibernate.getOther("banner").getContent();
        String Content = otherHibernate.getOther("partner").getContent();
        String Footer = otherHibernate.getOther("footer").getContent();
        Banner = Banner.replace("<p>","").replace("</p>","");
        Banner = removePreTag(Banner); Content = removePreTag(Content); Footer = removePreTag(Footer);

        insertBrands(model); insertPartners(model);
        model.addAttribute("Banner",Banner);
        model.addAttribute("Footer",Footer);
        model.addAttribute("Content", Content);
        return "web/partners";
    }

    @RequestMapping(value = {"/news"}, method = RequestMethod.GET)
    public String newsPage(ModelMap model){
        String Banner = otherHibernate.getOther("banner").getContent();
        String Footer = otherHibernate.getOther("footer").getContent();
        Banner = Banner.replace("<p>","").replace("</p>","");
        Banner = removePreTag(Banner); Footer = removePreTag(Footer);

        List<NewsView> news = newsHibernate.getAllNews();

        insertBrands(model);
        model.addAttribute("AllNews", news);
        model.addAttribute("Banner",Banner);
        model.addAttribute("Footer",Footer);
        return "web/news";
    }

    @RequestMapping(value = {"/news-details"}, method=RequestMethod.GET, params = "id")
    public String newsDetailPage(@RequestParam("id") Integer id, ModelMap model){
        String Banner = otherHibernate.getOther("banner").getContent();
        String Footer = otherHibernate.getOther("footer").getContent();
        Banner = Banner.replace("<p>","").replace("</p>","");
        Banner = removePreTag(Banner); Footer = removePreTag(Footer);

        News news = newsHibernate.getNews(id);
        String addedDate = newsHibernate.getDateFormat(id);

        insertBrands(model);
        model.addAttribute("AddedDate", addedDate);
        model.addAttribute("news", news);
        model.addAttribute("Banner",Banner);
        model.addAttribute("Footer",Footer);
        return "web/news-details";}

    @RequestMapping(value = {"/recruitment"}, method = RequestMethod.GET)
    public String recruitmentPage(ModelMap model){
        String Banner = otherHibernate.getOther("banner").getContent();
        String Content = otherHibernate.getOther("recruit").getContent();
        String Footer = otherHibernate.getOther("footer").getContent();
        Banner = Banner.replace("<p>","").replace("</p>","");
        Banner = removePreTag(Banner); Content = removePreTag(Content); Footer = removePreTag(Footer);

        insertBrands(model);
        model.addAttribute("Banner",Banner);
        model.addAttribute("Footer",Footer);
        model.addAttribute("Content", Content);
        return "web/recruitment";
    }

    @RequestMapping(value = {"/contact"}, method = RequestMethod.GET)
    public String contactPage(ModelMap model){
        String Banner = otherHibernate.getOther("banner").getContent();
        String Content = otherHibernate.getOther("contact").getContent();
        String Footer = otherHibernate.getOther("footer").getContent();
        Banner = Banner.replace("<p>","").replace("</p>","");
        Banner = removePreTag(Banner); Content = removePreTag(Content); Footer = removePreTag(Footer);

        insertBrands(model);
        model.addAttribute("insertFeedbackForm", new InsertFeedbackForm());
        model.addAttribute("Banner",Banner);
        model.addAttribute("Footer",Footer);
        model.addAttribute("Content", Content);
        return "web/contact";
    }

    @RequestMapping(value = {"/contact"}, method = RequestMethod.POST)
    public String insertFeedback(HttpServletRequest request, HttpServletResponse response, @Valid InsertFeedbackForm insertFeedbackForm, BindingResult result, ModelMap model){
        if(result.hasErrors()){
            return "web/contact";
        } else{
            try {
                insertFeedbackForm.setName(convertToUTF8(insertFeedbackForm.getName()));
                insertFeedbackForm.setContent(convertToUTF8(insertFeedbackForm.getContent()));
                insertFeedbackForm.setPhone(convertToUTF8(insertFeedbackForm.getPhone()));
                insertFeedbackForm.setEmail(convertToUTF8(insertFeedbackForm.getEmail()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            Feedback feedback = new Feedback();;

            feedback.setName(insertFeedbackForm.getName());
            feedback.setPhone(insertFeedbackForm.getPhone());
            feedback.setEmail(insertFeedbackForm.getEmail());
            feedback.setContent(insertFeedbackForm.getContent());
            feedbackHibernate.saveFeedback(feedback);
            return "redirect:/web/thankyou";
        }
    }

    @RequestMapping(value = {"/thankyou"}, method = RequestMethod.GET)
    public String thankyouPage(ModelMap model){
        String Banner = otherHibernate.getOther("banner").getContent();
        String Content = otherHibernate.getOther("recruit").getContent();
        String Footer = otherHibernate.getOther("footer").getContent();
        Banner = Banner.replace("<p>","").replace("</p>","");
        Banner = removePreTag(Banner); Content = removePreTag(Content); Footer = removePreTag(Footer);

        insertBrands(model);
        model.addAttribute("Banner",Banner);
        model.addAttribute("Footer",Footer);
        model.addAttribute("Content", Content);
        return "web/thankyou";
    }
}
